#ifndef ETHRLYREADER_H
#define ETHRLYREADER_H

#include <QThread>
#include "ethrly.h"

class EthRlyReader : public QThread
{
    Q_OBJECT
public:
    EthRlyReader(QString board_address, quint16 board_port);

    EthRly board;

protected:
    void run();

};

#endif // ETHRLYREADER_H
