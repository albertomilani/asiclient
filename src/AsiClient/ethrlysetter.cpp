#include "ethrlysetter.h"

EthRlySetter::EthRlySetter(QString board_address, quint16 board_port, quint8 requested_relay, bool requested_status) : QThread()
  , board(board_address, board_port)
{

    relay_num = requested_relay;
    relay_status = requested_status;
}

void EthRlySetter::run() {

    board.connect();
    if (relay_status) {
        board.turnRelayOn(relay_num);
    } else {
        board.turnRelayOff(relay_num);
    }
    board.disconnect();
}
