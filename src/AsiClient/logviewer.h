#ifndef LOGVIEWER_H
#define LOGVIEWER_H

#include <QWidget>

namespace Ui {
class LogViewer;
}

class LogViewer : public QWidget
{
    Q_OBJECT

public:
    explicit LogViewer(QWidget *parent = nullptr);
    ~LogViewer();

    enum LogLevel {
        TRACE = 10,
        DEBUG = 20,
        INFO = 30,
        WARNING = 40,
        ERROR = 50
    };

    void logTrace(QString msg);
    void logDebug(QString msg);
    void logInfo(QString msg);
    void logWarning(QString msg);
    void logError(QString msg);

protected:
    LogLevel defaultLogLevel;

    void setLogLevel(LogLevel level);

private:
    Ui::LogViewer *ui;

    void logMessage(LogLevel level, QString msg);

private slots:
    void updateDefaultLogLevel(int index);
};

#endif // LOGVIEWER_H
