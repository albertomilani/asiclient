#ifndef ADJUSTMENTPANEL_H
#define ADJUSTMENTPANEL_H

#include <QWidget>
#include "asiclient.h"

namespace Ui {
class AdjustmentPanel;
}

class AdjustmentPanel : public QWidget
{
    Q_OBJECT

public:
    explicit AdjustmentPanel(AsiClient *asi_client, QJsonObject *app_configuration, QWidget *parent = nullptr);
    ~AdjustmentPanel();

private:
    Ui::AdjustmentPanel *ui;

    QJsonObject *configuration;
};

#endif // ADJUSTMENTPANEL_H
