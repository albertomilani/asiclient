#include "ethrly.h"
#include <QByteArray>

EthRly::EthRly(QString board_address, quint16 board_port, QObject *parent) : QObject(parent)
{
    ipAddress = board_address;
    port = board_port;

    lastBoardStatus = BoardStatus();

    socket = NULL;

    endWithTimeout = false;
}

EthRly::~EthRly() {

}

void EthRly::connect() {

    if (socket == NULL) {
        socket = new QTcpSocket();
    }
    socket->connectToHost(ipAddress, port);
    if (!socket->waitForConnected(timeout)) {
        // emit error(socket->error(), socket->errorString());
        socket->abort();
    }
}

void EthRly::disconnect() {

    if (socket != NULL) {
        socket->disconnectFromHost();
    }
}

QByteArray EthRly::write(QByteArray command, bool expected_output = true) {

    QByteArray res;

    socket->write(command, qstrlen(command));
    if (expected_output && socket->waitForReadyRead(timeout)) {
        res = socket->readAll();
        endWithTimeout = false;
    } else {
        endWithTimeout = true;
    }
    QByteArray out(1, 0x0);
    socket->write(out, qstrlen(out));

    return res;
}

void EthRly::turnRelayOn(quint8 relay_num) {

    QByteArray command = QByteArray(1, 0x64 + relay_num);
    write(command);
}

void EthRly::turnRelayOff(quint8 relay_num) {

    QByteArray command = QByteArray(1, 0x6E + relay_num);
    write(command);
}

EthRly::BoardStatus EthRly::getRelayStatus() {

    BoardStatus currentBoardStatus;

    QByteArray status = write(QByteArray(1, 0x5B), true);

    if (!endWithTimeout) {
        currentBoardStatus.relay0 = RelayStatus(status[0] & 0b00000001);
        currentBoardStatus.relay1 = RelayStatus((status[0] & 0b00000010) >> 1);
        currentBoardStatus.relay2 = RelayStatus((status[0] & 0b00000100) >> 2);
        currentBoardStatus.relay3 = RelayStatus((status[0] & 0b00001000) >> 3);
        currentBoardStatus.relay4 = RelayStatus((status[0] & 0b00010000) >> 4);
        currentBoardStatus.relay5 = RelayStatus((status[0] & 0b00100000) >> 5);
        currentBoardStatus.relay6 = RelayStatus((status[0] & 0b01000000) >> 6);
        currentBoardStatus.relay7 = RelayStatus((status[0] & 0b10000000) >> 7);

        if (currentBoardStatus != lastBoardStatus) {
            emit boardStatusChanged(currentBoardStatus);
        }
    }

    lastBoardStatus = currentBoardStatus;

    return currentBoardStatus;
}
