#include "telescope_message.h"
#include <cstring>

TelescopeNetMessage::TelescopeNetMessage()
{

    message = new Message();

    // set default values
    message->stx = 0xAAAAAAAA;
    message->type = 0;
    message->msrc = 0;
    message->mdest = 0;
    //message->chan = NULL;
    message->chan = 0;
    message->crc = 0;
    message->hcrc = 0;
}

QByteArray TelescopeNetMessage::getMessage() {

    QByteArray data((char*)message, sizeof(Message));
    if (message->size) {
        data.append(messagePayload.toUtf8().append('\0'));
    }
    return data;
}

int TelescopeNetMessage::getSize() {
    return message->size;
}

void TelescopeNetMessage::setStx(int stx) {
    message->stx = stx;
}

void TelescopeNetMessage::setType(int type) {
    message->type = type;
}

void TelescopeNetMessage::setMsrc(int msrc) {
    message->msrc = msrc;
}

void TelescopeNetMessage::setPsrc(QString psrc) {
    std::strcpy(message->psrc, psrc.toUtf8().constData());
}

void TelescopeNetMessage::setMdest(int mdest) {
    message->mdest = mdest;
}

void TelescopeNetMessage::setPdest(QString pdest) {
    std::strcpy(message->pdest, pdest.toUtf8().constData());
}

void TelescopeNetMessage::setCommand(Instructions command) {
    message->code = command;
}

void TelescopeNetMessage::setPayload(QString payload) {
    if (payload.length() > 0) {
        message->size = payload.length() + 1;
        if (message->size) {
            messagePayload = payload;
        }
    }
}

void TelescopeNetMessage::setCrc() {
    message->crc = messageCrc(reinterpret_cast<unsigned char*>(messagePayload.toUtf8().data()), message->size);
    message->hcrc = messageCrc(reinterpret_cast<unsigned char*>(message), sizeof(Message)-sizeof(int));
}

void TelescopeNetMessage::setMessageFromBuffer(QByteArray *buffer) {
    message = reinterpret_cast<Message*>(buffer);
}

int TelescopeNetMessage::messageCrc(unsigned char *b, int n) {

    uint i;
    uint *x, crc;

    x = (uint*)b;
    crc = 0;

    for (i = 0; i < n/sizeof(uint); i++) {
        crc = crc ^ x[i];
    }
    for (i = 0; i < n%sizeof(uint); i++) {
        crc = crc ^ b[n-i-1];
    }

    return crc;
}
