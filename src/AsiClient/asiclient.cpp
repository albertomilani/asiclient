#include "asiclient.h"
#include "ui_asiclient.h"
#include "adjustmentpanel.h"
#include "autoguidepanel.h"
#include "logviewer.h"
#include "imagelabel.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QPainter>
#include <QFileDialog>
#include <QDebug>

#define IMAGE_MAX_RANGE 255
#define DEV true

AsiClient::AsiClient(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::AsiClient)
    , spectrograph(NULL)
    , field(NULL)
    , dome(NULL)
    , adjustmentPanel(NULL)
    , autoGuidePanel(NULL)
    , logsPanel(NULL)
    , relayBoardReader(NULL)
    , relayBoardSetLampOn(NULL)
    , relayBoardSetLampOff(NULL)
    , crosshair1_params()
    , crosshair2_params()
    , fieldImageGridParams()
    , lastSpectrographImage()
    , lastFieldImage()
    , lastDomeImage()
    , spectrographFlipHorizontal(0)
    , spectrographFlipVertical(0)
    , fieldFlipHorizontal(0)
    , fieldFlipVertical(0)
{
    ui->setupUi(this);

    spectrograph_params = new AsiReader::AsiParams();
    field_params = new AsiReader::AsiParams();
    dome_params = new AsiReader::AsiParams();

    ui->sb_spectrographImageBlack->setMinimum(0);
    ui->sb_spectrographImageBlack->setMaximum(IMAGE_MAX_RANGE);
    ui->sb_spectrographImageBlack->setValue(0);

    ui->sb_spectrographImageRange->setMinimum(0);
    ui->sb_spectrographImageRange->setMaximum(IMAGE_MAX_RANGE);
    ui->sb_spectrographImageRange->setValue(IMAGE_MAX_RANGE);
}

AsiClient::~AsiClient()
{
    delete ui;
}

void AsiClient::closeEvent(QCloseEvent *event) {
    QMainWindow::closeEvent(event);
    emit exited();
}

void AsiClient::loadConfiguration() {

    QFile configFile;

    if (DEV) {
        configFile.setFileName("/home/milhouse/asiclient.conf");
    } else {
        configFile.setFileName("/home/osservatorio/asiclient.conf");
    }

    logsPanel->logDebug("Load configuration from " + configFile.fileName());

    // TODO check file exists
    configFile.open(QIODevice::ReadOnly|QIODevice::Text);
    configuration = QJsonDocument().fromJson(configFile.readAll()).object();
    configFile.close();

}

void AsiClient::saveConfiguration() {

    QFile configFile;

    if (DEV) {
        configFile.setFileName("/home/milhouse/asiclient.conf");
    } else {
        configFile.setFileName("/home/osservatorio/asiclient.conf");
    }

    logsPanel->logDebug("Save configuration to " + configFile.fileName());

    configFile.open(QIODevice::WriteOnly);
    QJsonDocument jsonDoc;
    jsonDoc.setObject(configuration);
    configFile.write(jsonDoc.toJson());
    configFile.close();
}

void AsiClient::configurationUpdated() {

    saveConfiguration();
}

void AsiClient::createMenus() {

    QMenu *fileMenu = new QMenu("&File");
    QMenu *toolsMenu = new QMenu("&Tools");

    QAction *saveSpectrographImageAction = new QAction("Save spectrograph image", this);
    QAction *saveFieldImageAction = new QAction("Save field image", this);
    QAction *saveDomeImageAction = new QAction("Save dome image", this);

    QAction *openAdjustmentPanelAction = new QAction("Adjustment", this);
    QAction *openAutoGuidePanelAction = new QAction("Auto guide", this);
    QAction *openLogsPanelAction = new QAction("Logs", this);

    menuBar()->addMenu(fileMenu);
    fileMenu->addAction(saveSpectrographImageAction);
    fileMenu->addAction(saveFieldImageAction);
    fileMenu->addAction(saveDomeImageAction);

    menuBar()->addMenu(toolsMenu);
    toolsMenu->addAction(openAdjustmentPanelAction);
    toolsMenu->addAction(openAutoGuidePanelAction);
    toolsMenu->addSeparator();
    toolsMenu->addAction(openLogsPanelAction);

    connect(saveSpectrographImageAction, &QAction::triggered, this, &AsiClient::saveSpectrographImage);
    connect(saveFieldImageAction, &QAction::triggered, this, &AsiClient::saveFieldImage);
    connect(saveDomeImageAction, &QAction::triggered, this, &AsiClient::saveDomeImage);

    connect(openAdjustmentPanelAction, &QAction::triggered, this, &AsiClient::openAdjustmentPanel);
    connect(openAutoGuidePanelAction, &QAction::triggered, this, &AsiClient::openAutoGuidePanel);
    connect(openLogsPanelAction, &QAction::triggered, this, &AsiClient::openLogsPanel);
}

void AsiClient::start() {

    createMenus();

    if (logsPanel == NULL) {
        logsPanel = new LogViewer();
    }

    loadConfiguration();

    if (adjustmentPanel == NULL) {
        adjustmentPanel = new AdjustmentPanel(this, &configuration);
    }

    // connect push buttons
    connect(ui->pb_crosshair1, &QPushButton::released, this, &AsiClient::updateCrosshair1Position);
    connect(ui->pb_crosshair2, &QPushButton::released, this, &AsiClient::updateCrosshair2Position);
    connect(ui->pb_fieldImageGrid, &QPushButton::released, this, &AsiClient::updateFieldImageGrid);
    connect(ui->pb_resetSpectrographImageLevels, &QPushButton::released, this, &AsiClient::resetSpectrographImageLevels);

    connect(ui->lb_spectrographImage, &ImageLabel::mouseCoords, this, &AsiClient::updateSpectrographMouseCoords);

    connect(ui->sb_spectrographImageBlack, SIGNAL(valueChanged(int)), this, SLOT(updateSpectrographImageRange(int)));

    if (configuration.value(CONFIG_KEY_CROSSHAIR1) != QJsonValue::Null) {
        QJsonObject crosshair1_config = configuration.value(CONFIG_KEY_CROSSHAIR1).toObject();
        crosshair1_params.x = crosshair1_config.value(CONFIG_KEY_CROSSHAIR_X).toInt();
        crosshair1_params.y = crosshair1_config.value(CONFIG_KEY_CROSSHAIR_Y).toInt();
        crosshair1_params.hole_width = crosshair1_config.value(CONFIG_KEY_CROSSHAIR_HOLE_WIDTH).toInt();
        ui->le_crosshair1_x->setText(QString::number(crosshair1_params.x));
        ui->le_crosshair1_y->setText(QString::number(crosshair1_params.y));
        ui->cb_crosshair1->setChecked(crosshair1_config.value(CONFIG_KEY_CROSSHAIR_ENABLED).toBool());
    } else {
        ui->cb_crosshair1->setChecked(true);
    }

    if (configuration.value(CONFIG_KEY_CROSSHAIR2) != QJsonValue::Null) {
        QJsonObject crosshair2_config = configuration.value(CONFIG_KEY_CROSSHAIR2).toObject();
        crosshair2_params.x = crosshair2_config.value(CONFIG_KEY_CROSSHAIR_X).toInt();
        crosshair2_params.y = crosshair2_config.value(CONFIG_KEY_CROSSHAIR_Y).toInt();
        crosshair2_params.hole_width = crosshair2_config.value(CONFIG_KEY_CROSSHAIR_HOLE_WIDTH).toInt();
        ui->le_crosshair2_x->setText(QString::number(crosshair2_params.x));
        ui->le_crosshair2_y->setText(QString::number(crosshair2_params.y));
        ui->cb_crosshair2->setChecked(crosshair2_config.value(CONFIG_KEY_CROSSHAIR_ENABLED).toBool());
    } else {
        ui->cb_crosshair2->setChecked(false);
    }

    if (configuration.value(CONFIG_KEY_FIELD_IMAGE_GRID) != QJsonValue::Null) {
        QJsonObject fieldImageGrid_config = configuration.value(CONFIG_KEY_FIELD_IMAGE_GRID).toObject();
        fieldImageGridParams.x_center = fieldImageGrid_config.value(CONFIG_KEY_FIELD_IMAGE_GRID_X_CENTER).toInt();
        fieldImageGridParams.y_center = fieldImageGrid_config.value(CONFIG_KEY_FIELD_IMAGE_GRID_Y_CENTER).toInt();
        fieldImageGridParams.h_spacing = fieldImageGrid_config.value(CONFIG_KEY_FIELD_IMAGE_GRID_H_SPACING).toInt();
        fieldImageGridParams.v_spacing = fieldImageGrid_config.value(CONFIG_KEY_FIELD_IMAGE_GRID_V_SPACING).toInt();
        ui->le_fieldImageGrid_x->setText(QString::number(fieldImageGridParams.x_center));
        ui->le_fieldImageGrid_y->setText(QString::number(fieldImageGridParams.y_center));
        ui->cb_fieldImageGrid->setChecked(fieldImageGrid_config.value(CONFIG_KEY_FIELD_IMAGE_GRID_ENABLED).toBool());
    } else {
        ui->cb_fieldImageGrid->setChecked(false);
    }

    if (configuration.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP) != QJsonValue::Null) {
        QJsonObject spectrographFlip_config = configuration.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP).toObject();
        spectrographFlipHorizontal = spectrographFlip_config.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_HORIZONTAL).toInt();
        spectrographFlipVertical = spectrographFlip_config.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_VERTICAL).toInt();
    }

    if (configuration.value(CONFIG_KEY_FIELD_IMAGE_FLIP) != QJsonValue::Null) {
        QJsonObject fieldFlip_config = configuration.value(CONFIG_KEY_FIELD_IMAGE_FLIP).toObject();
        fieldFlipHorizontal = fieldFlip_config.value(CONFIG_KEY_FIELD_IMAGE_FLIP_HORIZONTAL).toInt();
        fieldFlipVertical = fieldFlip_config.value(CONFIG_KEY_FIELD_IMAGE_FLIP_VERTICAL).toInt();
    }


    if (configuration.value(CONFIG_KEY_CAMERAS) != QJsonValue::Null) {

        QJsonObject camera_config = configuration.value(CONFIG_KEY_CAMERAS).toObject();

        if (configuration.value(CONFIG_KEY_CAMERA_SPECTROGRAPH) != QJsonValue::Null) {
            QJsonValue address = camera_config.value(CONFIG_KEY_CAMERA_SPECTROGRAPH).toObject().value(CONFIG_KEY_CAMERA_SPECTROGRAPH_IP_ADDRESS);
            QJsonValue port = camera_config.value(CONFIG_KEY_CAMERA_SPECTROGRAPH).toObject().value(CONFIG_KEY_CAMERA_SPECTROGRAPH_PORT);
            bool camera_enabled = camera_config.value(CONFIG_KEY_CAMERA_SPECTROGRAPH).toObject().value(CONFIG_KEY_CAMERA_SPECTROGRAPH_ENABLED).toBool();

            if (camera_enabled && address != QJsonValue::Null && port != QJsonValue::Null) {
                spectrograph = new AsiReader(address.toString(), (quint16)(port.toDouble()), spectrograph_params);
                connect(spectrograph, &AsiReader::imageReceived, this, &AsiClient::getSpectrographImage);
                spectrograph->start();

                if (autoGuidePanel == NULL) {
                    autoGuidePanel = new AutoGuidePanel(spectrograph, &configuration, logsPanel);
                    connect(autoGuidePanel, &AutoGuidePanel::configurationChanged, this, &AsiClient::configurationUpdated);
                }
            }
        }

        if (configuration.value(CONFIG_KEY_CAMERA_FIELD) != QJsonValue::Null) {
            QJsonValue address = camera_config.value(CONFIG_KEY_CAMERA_FIELD).toObject().value(CONFIG_KEY_CAMERA_FIELD_IP_ADDRESS);
            QJsonValue port = camera_config.value(CONFIG_KEY_CAMERA_FIELD).toObject().value(CONFIG_KEY_CAMERA_FIELD_PORT);
            bool camera_enabled = camera_config.value(CONFIG_KEY_CAMERA_FIELD).toObject().value(CONFIG_KEY_CAMERA_FIELD_ENABLED).toBool();

            if (camera_enabled && address != QJsonValue::Null && port != QJsonValue::Null) {
                field = new AsiReader(address.toString(), (quint16)(port.toDouble()), field_params);
                connect(field, &AsiReader::imageReceived, this, &AsiClient::getFieldImage);
                field->start();
            }
        }

        if (configuration.value(CONFIG_KEY_CAMERA_DOME) != QJsonValue::Null) {
            QJsonValue address = camera_config.value(CONFIG_KEY_CAMERA_DOME).toObject().value(CONFIG_KEY_CAMERA_DOME_IP_ADDRESS);
            QJsonValue port = camera_config.value(CONFIG_KEY_CAMERA_DOME).toObject().value(CONFIG_KEY_CAMERA_DOME_PORT);
            bool camera_enabled = camera_config.value(CONFIG_KEY_CAMERA_DOME).toObject().value(CONFIG_KEY_CAMERA_DOME_ENABLED).toBool();

            if (camera_enabled && address != QJsonValue::Null && port != QJsonValue::Null) {
                dome = new AsiReader(address.toString(), (quint16)(port.toDouble()), dome_params);
                connect(dome, &AsiReader::imageReceived, this, &AsiClient::getDomeImage);
                dome->start();
            }
        }
    }

    if (configuration.value(CONFIG_KEY_RELAY_BOARD) != QJsonValue::Null) {

        QString address = configuration.value(CONFIG_KEY_RELAY_BOARD).toObject().value(CONFIG_KEY_RELAY_BOARD_IP_ADDRESS).toString();
        quint16 port = configuration.value(CONFIG_KEY_RELAY_BOARD).toObject().value(CONFIG_KEY_RELAY_BOARD_PORT).toInt();

        if (relayBoardSetLampOn == NULL) {
            relayBoardSetLampOn = new EthRlySetter(address, port, 1, true);
            connect(ui->pb_lampPowerOn, &QPushButton::released, this, &AsiClient::setLampRelayOn);
        }
        if (relayBoardSetLampOff == NULL) {
            relayBoardSetLampOff = new EthRlySetter(address, port, 1, false);
            connect(ui->pb_lampPowerOff, &QPushButton::released, this, &AsiClient::setLampRelayOff);
        }


        relayBoardReader = new EthRlyReader(address, port);
        connect(&relayBoardReader->board, &EthRly::boardStatusChanged, this, &AsiClient::updateLampStatus);
        relayBoardReader->start();
    }

}

void AsiClient::updateSpectrographMouseCoords(QPointF point) {

    quint32 x = (double)point.x() * (1280.0/600.0);
    quint32 y = (double)(600 - point.y()) * (1280.0/600.0);

    // REMEMBER: now image is rotated!
    ui->lb_spectrographMouseX->setText(QString::number(y));
    ui->lb_spectrographMouseY->setText(QString::number(x));
}

void AsiClient::updateCrosshair1Position() {

    crosshair1_params.x = ui->le_crosshair1_x->text().toUInt();
    crosshair1_params.y = ui->le_crosshair1_y->text().toUInt();

    QJsonObject crosshair_config = configuration.value(CONFIG_KEY_CROSSHAIR1).toObject();

    crosshair_config.insert(CONFIG_KEY_CROSSHAIR_X, QJsonValue((int)crosshair1_params.x));
    crosshair_config.insert(CONFIG_KEY_CROSSHAIR_Y, QJsonValue((int)crosshair1_params.y));

    configuration.insert(CONFIG_KEY_CROSSHAIR1, QJsonValue(crosshair_config));

    saveConfiguration();

}

void AsiClient::updateCrosshair2Position(){

    crosshair2_params.x = ui->le_crosshair2_x->text().toUInt();
    crosshair2_params.y = ui->le_crosshair2_y->text().toUInt();

    QJsonObject crosshair_config = configuration.value(CONFIG_KEY_CROSSHAIR2).toObject();

    crosshair_config.insert(CONFIG_KEY_CROSSHAIR_X, QJsonValue((int)crosshair2_params.x));
    crosshair_config.insert(CONFIG_KEY_CROSSHAIR_Y, QJsonValue((int)crosshair2_params.y));

    configuration.insert(CONFIG_KEY_CROSSHAIR2, QJsonValue(crosshair_config));

    saveConfiguration();
}

void AsiClient::updateFieldImageGrid() {

    fieldImageGridParams.x_center = ui->le_fieldImageGrid_x->text().toUInt();
    fieldImageGridParams.y_center = ui->le_fieldImageGrid_y->text().toUInt();

    QJsonObject grid_config = configuration.value(CONFIG_KEY_FIELD_IMAGE_GRID).toObject();

    grid_config.insert(CONFIG_KEY_FIELD_IMAGE_GRID_X_CENTER, QJsonValue((int)fieldImageGridParams.x_center));
    grid_config.insert(CONFIG_KEY_FIELD_IMAGE_GRID_X_CENTER, QJsonValue((int)fieldImageGridParams.y_center));

    configuration.insert(CONFIG_KEY_FIELD_IMAGE_GRID, QJsonValue(grid_config));

    saveConfiguration();
}

void AsiClient::updateSpectrographFlipHorizontal(int state) {

    QJsonObject flip_config;

    spectrographFlipHorizontal = state;

    if (autoGuidePanel != NULL) {
        autoGuidePanel->setSpectrographFlipHorizontal(state);
    }

    if (configuration.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP) != QJsonValue::Null) {
        flip_config = configuration.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP).toObject();
    }
    
    flip_config.insert(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_HORIZONTAL, QJsonValue((int)state));

    configuration.insert(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP, QJsonValue(flip_config));

    saveConfiguration();
}

void AsiClient::updateSpectrographFlipVertical(int state) {

    QJsonObject flip_config;

    spectrographFlipVertical = state;

    if (autoGuidePanel != NULL) {
        autoGuidePanel->setSpectrographFlipVertical(state);
    }

    if (configuration.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP) != QJsonValue::Null) {
        flip_config = configuration.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP).toObject();
    }
    
    flip_config.insert(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_VERTICAL, QJsonValue((int)state));

    configuration.insert(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP, QJsonValue(flip_config));

    saveConfiguration();
}

void AsiClient::updateFieldFlipHorizontal(int state) {
    
    QJsonObject flip_config;

    fieldFlipHorizontal = state;

    if (configuration.value(CONFIG_KEY_FIELD_IMAGE_FLIP) != QJsonValue::Null) {
        flip_config = configuration.value(CONFIG_KEY_FIELD_IMAGE_FLIP).toObject();
    }
    
    flip_config.insert(CONFIG_KEY_FIELD_IMAGE_FLIP_HORIZONTAL, QJsonValue((int)state));

    configuration.insert(CONFIG_KEY_FIELD_IMAGE_FLIP, QJsonValue(flip_config));

    saveConfiguration();
}

void AsiClient::updateFieldFlipVertical(int state) {

    QJsonObject flip_config;

    fieldFlipVertical = state;

    if (configuration.value(CONFIG_KEY_FIELD_IMAGE_FLIP) != QJsonValue::Null) {
        flip_config = configuration.value(CONFIG_KEY_FIELD_IMAGE_FLIP).toObject();
    }
    
    flip_config.insert(CONFIG_KEY_FIELD_IMAGE_FLIP_VERTICAL, QJsonValue((int)state));

    configuration.insert(CONFIG_KEY_FIELD_IMAGE_FLIP, QJsonValue(flip_config));

    saveConfiguration();
}

void AsiClient::updateLampStatus(EthRly::BoardStatus boardStatus) {

    if (boardStatus.relay0 == 0) {
        ui->lb_lampStatus->setText("OFF");
        ui->lb_lampStatus->setStyleSheet("QLabel { background-color : red;}");
    }
    if (boardStatus.relay0 == 1) {
        ui->lb_lampStatus->setText("ON");
        ui->lb_lampStatus->setStyleSheet("QLabel { background-color : green;}");
    }
}

void AsiClient::setLampRelayOn() {
    if (relayBoardSetLampOn != NULL && !relayBoardSetLampOn->isRunning()) {
        relayBoardSetLampOn->start();
    }

}

void AsiClient::setLampRelayOff() {
    if (relayBoardSetLampOff != NULL && !relayBoardSetLampOff->isRunning()) {
        relayBoardSetLampOff->start();
    }
}

void AsiClient::saveSpectrographImage() {

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    "/home/osservatorio/spectrograph.png",
                                                    tr("PNG Images (*.png)"));
    lastSpectrographImage.save(fileName, "PNG");
}

void AsiClient::saveFieldImage() {

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    "/home/osservatorio/field.png",
                                                    tr("PNG Images (*.png)"));
    lastFieldImage.save(fileName, "PNG");
}

void AsiClient::saveDomeImage() {

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    "/home/osservatorio/dome.png",
                                                    tr("PNG Images (*.png)"));
    lastDomeImage.save(fileName, "PNG");
}

void AsiClient::openAdjustmentPanel() {

    adjustmentPanel->setVisible(true);
}

void AsiClient::openAutoGuidePanel() {

    autoGuidePanel->setVisible(true);
}

void AsiClient::openLogsPanel() {

    logsPanel->setVisible(true);
}

void AsiClient::getSpectrographExpTime(int value) {
    spectrograph_params->exp_time = value;
}

void AsiClient::getSpectrographGain(int value) {
    spectrograph_params->gain = value;
}

void AsiClient::getFieldExpTime(int value) {
    field_params->exp_time = value;
}

void AsiClient::getFieldGain(int value) {
    field_params->gain = value;
}

void AsiClient::getDomeExpTime(int value) {
    dome_params->exp_time = value;
}

void AsiClient::getDomeGain(int value) {
    dome_params->gain = value;
}

void AsiClient::getSpectrographImage(QByteArray data) {

    QImage img = QImage((const uchar*)data.data(), 1280, 960, QImage::Format_Grayscale8);
    lastSpectrographImage = QPixmap::fromImage(img);

    QImage img_scaled = img.scaled(QSize(600, 450), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    quint32 black = ui->sb_spectrographImageBlack->value();
    quint32 range = ui->sb_spectrographImageRange->value();
    QImage img_stretched = setImageLevels(img_scaled, black, range);

    QPixmap imgPreFlip = QPixmap::fromImage(img_stretched);

    QTransform flipTransformation;

    if (spectrographFlipHorizontal != 0) {
        flipTransformation.scale(1,-1);
    }
    if (spectrographFlipVertical != 0) {
        flipTransformation.scale(-1,1);
    }

    QPixmap img_pixmap = imgPreFlip.transformed(flipTransformation);

    if (ui->cb_crosshair1->isChecked()) {

        quint32 x1 = (double)crosshair1_params.x / (1280.0/600.0);
        quint32 y1 = (double)crosshair1_params.y / (1280.0/600.0);

        QPainter crosshari1Painter(&img_pixmap);
        crosshari1Painter.setPen("#ff0000");
        crosshari1Painter.drawLine(x1, 0, x1, y1-crosshair1_params.hole_width);
        crosshari1Painter.drawLine(x1, y1+crosshair1_params.hole_width, x1, 450);
        crosshari1Painter.drawLine(0, y1, x1-crosshair1_params.hole_width, y1);
        crosshari1Painter.drawLine(x1+crosshair1_params.hole_width, y1, 600, y1);
    }

    if (ui->cb_crosshair2->isChecked()) {

        quint32 x2 = (double)crosshair2_params.x / (1280.0/600.0);
        quint32 y2 = (double)crosshair2_params.y / (1280.0/600.0);

        QPainter crosshari2Painter(&img_pixmap);
        crosshari2Painter.setPen("#00ff00");
        crosshari2Painter.drawLine(x2, 0, x2, y2-crosshair2_params.hole_width);
        crosshari2Painter.drawLine(x2, y2+crosshair2_params.hole_width, x2, 450);
        crosshari2Painter.drawLine(0, y2, x2-crosshair2_params.hole_width, y2);
        crosshari2Painter.drawLine(x2+crosshair2_params.hole_width, y2, 600, y2);
    }

    if (autoGuidePanel != NULL && autoGuidePanel->isVisible()) {

        quint32 x_center = (double)autoGuidePanel->getBoxXCenter() / (1280.0/600.0);
        quint32 y_center = (double)autoGuidePanel->getBoxYCenter() / (1280.0/600.0);
        quint32 innerBoxSize = (double)autoGuidePanel->getBoxInnerSize() / (1280.0/600.0);
        quint32 outerBoxSize = (double)autoGuidePanel->getBoxOuterSize() / (1280.0/600.0);

        QPainter autoguideBoxes(&img_pixmap);
        autoguideBoxes.setPen("#00ff00");
        autoguideBoxes.drawRect(x_center - innerBoxSize/2, y_center - innerBoxSize/2, innerBoxSize, innerBoxSize);
        autoguideBoxes.setPen("#00ffff");
        autoguideBoxes.drawRect(x_center - outerBoxSize/2, y_center - outerBoxSize/2, outerBoxSize, outerBoxSize);
    }

    // rotate image just before drawing
    QTransform transformation;
    transformation.rotate(90);
    QPixmap img_rotated = img_pixmap.transformed(transformation);

    ui->lb_spectrographImage->setPixmap(img_rotated);
    ui->lb_spectrographImage->update();

    QRect rect(565, 405, 150, 150);
    QPixmap cropped = QPixmap::fromImage(img).transformed(flipTransformation).copy(rect);

    QPixmap cropped_rotated = cropped.transformed(transformation);
    ui->lb_spectrographCroppedImage->setPixmap(cropped_rotated);
    ui->lb_spectrographCroppedImage->update();

}

void AsiClient::getFieldImage(QByteArray data) {

    QImage img = QImage((const uchar*)data.data(), 1280, 960, QImage::Format_Grayscale8);
    lastFieldImage = QPixmap::fromImage(img);

    QImage img_scaled = img.scaled(QSize(600, 450), Qt::KeepAspectRatio, Qt::SmoothTransformation);

    QPixmap img_pixmap = QPixmap::fromImage(img_scaled);

    QTransform transformation;

    if (fieldFlipHorizontal != 0) {
        transformation.scale(-1,1);
    }
    if (fieldFlipVertical != 0) {
        transformation.scale(1,-1);
    }

    img_pixmap = img_pixmap.transformed(transformation);

    if (ui->cb_fieldImageGrid->isChecked()) {

        quint32 x_center = (double)fieldImageGridParams.x_center / (1280.0/600.0);
        quint32 y_center = (double)fieldImageGridParams.y_center / (1280.0/600.0);
        quint32 h_spacing = (double)fieldImageGridParams.h_spacing / (1280.0/600.0);
        quint32 v_spacing = (double)fieldImageGridParams.v_spacing / (1280.0/600.0);

        QPainter fieldImageGridPainter(&img_pixmap);
        fieldImageGridPainter.setPen("#00ffff");
        QPen pen = fieldImageGridPainter.pen();
        pen.setStyle(Qt::DotLine);
        fieldImageGridPainter.setPen(pen);
        quint32 x;
        x = x_center % h_spacing;
        while (x < 600) {
            fieldImageGridPainter.drawLine(x, 0, x, 450);
            x += h_spacing;
        }
        quint32 y;
        y = y_center % v_spacing;
        while (y < 450) {
            fieldImageGridPainter.drawLine(0, y, 600, y);
            y += v_spacing;
        }
    }

    ui->lb_fieldImage->setPixmap(img_pixmap);
    ui->lb_fieldImage->update();
}

void AsiClient::getDomeImage(QByteArray data) {

    QImage img = QImage((const uchar*)data.data(), 1280, 960, QImage::Format_Grayscale8);
    lastDomeImage = QPixmap::fromImage(img);

    QImage img_scaled = img.scaled(QSize(600, 450), Qt::KeepAspectRatio, Qt::SmoothTransformation);

    ui->lb_domeImage->setPixmap(QPixmap::fromImage(img_scaled));
    ui->lb_domeImage->update();
}

void AsiClient::resetSpectrographImageLevels() {

    ui->sb_spectrographImageBlack->setValue(0);
    ui->sb_spectrographImageRange->setValue(IMAGE_MAX_RANGE);
}

void AsiClient::updateSpectrographImageRange(int value) {

    ui->sb_spectrographImageRange->setMinimum(value + 1);
}

QImage AsiClient::setImageLevels(QImage image, quint32 black, quint32 range) {

    QColor color;
    quint32 level;

    // do not loop for nothing
    if (black != 0 || range != 255) {

        for (int i=0; i<image.width(); i++) {
            for(int j=0; j<image.height(); j++) {

                color = image.pixel(i, j);
                level = color.lightness();
                if (level < black) {
                    level = 0;
                } else {
                    level = color.red() - black;
                }
                level = level * ((IMAGE_MAX_RANGE - black)/(range - black));
                image.setPixel(i, j, qRgb(level, level, level));
            }
        }
    }

    return image;
}
