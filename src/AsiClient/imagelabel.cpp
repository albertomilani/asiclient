#include "imagelabel.h"
#include <QMouseEvent>
#include <QDebug>

ImageLabel::ImageLabel(QWidget *parent) : QLabel(parent)
{

}

ImageLabel::~ImageLabel() {

}

void ImageLabel::mousePressEvent(QMouseEvent *event) {

    emit mouseCoords(event->localPos());
}
