#ifndef ASIREADER_H
#define ASIREADER_H

#include <QThread>
#include <QTcpSocket>


class AsiReader : public QThread
{
    Q_OBJECT

public:

    struct AsiParams {
        qint32 exp_time;
        qint16 gain;

        AsiParams() {
            exp_time = 30;
            gain = 150;
        }
    };

    AsiReader(QString address, quint16 port, AsiParams *parameters);

    AsiParams *params;

private:
    QString _address;
    qint16 _port;

    QTcpSocket *socket;

    QByteArray data;

protected:
    void run();

signals:
    void error(int socketError, const QString &message);
    void imageReceived(QByteArray data);
};

#endif // ASIREADER_H
