#include "telescope_connector.h"

TelescopeConnector::TelescopeConnector(QString address, quint16 port, LogViewer *mainLogger) : QThread()
  , logger(mainLogger)
  , telescopeAddress(address)
  , telescopePort(port)
  , telescopeSocket(NULL)
  , requestedDisconnection(false)
{
    qRegisterMetaType<TelescopeNetMessage::Message>("TelescopeNetMessage::Message");
}

void TelescopeConnector::newMessage(TelescopeNetMessage *message) {

    if (telescopeSocket != NULL && telescopeSocket->state() == QAbstractSocket::ConnectedState) {
        messageQueue.enqueue(message);
    }
}

void TelescopeConnector::requestDisconnection() {
    requestedDisconnection = true;
}

void TelescopeConnector::run() {

    while (true) {

        if (requestedDisconnection) {
            if (telescopeSocket != NULL) {
                telescopeSocket->disconnectFromHost();
                terminate();
            }
        }

        if (telescopeSocket == NULL || telescopeSocket->state() != QAbstractSocket::ConnectedState) {

            logger->logTrace("(Telescope connector) Connect to telescope");

            telescopeSocket = new QTcpSocket();
            telescopeSocket->connectToHost(telescopeAddress, telescopePort);

            if (!telescopeSocket->waitForConnected(10000)) {
                logger->logWarning("(Telescope connector) Cannot connect to telescope");
                QThread::msleep(500);
                continue;
            }

            messageQueue = QQueue<TelescopeNetMessage*>();

            logger->logTrace("(Telescope connector) Telescope connected");

            emit telescopeConnected();
        }

        // get element from queue and write commands
        if (!messageQueue.isEmpty()) {

            logger->logDebug("(Telescope connector) Dequeue new message");

            TelescopeNetMessage *mess = messageQueue.dequeue();
            QByteArray msg = mess->getMessage();
            telescopeSocket->write(msg);
            telescopeSocket->flush();

            if (telescopeSocket->waitForBytesWritten(100)) {

            }

            delete(mess);

        } else {

        }

        // read from telescope

        QByteArray recv;

        while (telescopeSocket->waitForReadyRead(100)) {

            logger->logTrace("(Telescope connector) Bytes available");

            QByteArray in_data;
            in_data = telescopeSocket->readAll();
            recv = recv + in_data;

        }

        if (recv.length()) {
            emit telescopeMessageAvailable(recv);
        }

        QThread::msleep(100);
    }


}
