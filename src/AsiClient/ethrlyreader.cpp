#include "ethrlyreader.h"

EthRlyReader::EthRlyReader(QString board_address, quint16 board_port) : QThread()
  , board(board_address, board_port)
{

}

void EthRlyReader::run() {

    while (true) {
        board.connect();
        board.getRelayStatus();
        board.disconnect();
        msleep(200);
    }
}
