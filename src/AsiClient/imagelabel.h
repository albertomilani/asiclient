#ifndef IMAGELABEL_H
#define IMAGELABEL_H

#include <QLabel>

class ImageLabel : public QLabel
{
    Q_OBJECT

public:
    ImageLabel(QWidget* parent = nullptr);
    ~ImageLabel();

protected:
    void mousePressEvent(QMouseEvent *event);

signals:
    void mouseCoords(QPointF point);
};

#endif // IMAGELABEL_H
