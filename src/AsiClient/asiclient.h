#ifndef ASICLIENT_H
#define ASICLIENT_H

#include <QMainWindow>
#include <QJsonObject>
#include "asireader.h"
#include "ethrly.h"
#include "ethrlyreader.h"
#include "ethrlysetter.h"
#include "config_keys.h"

class AdjustmentPanel;
class AutoGuidePanel;
class LogViewer;

QT_BEGIN_NAMESPACE
namespace Ui { class AsiClient; }
QT_END_NAMESPACE

class AsiClient : public QMainWindow
{
    Q_OBJECT

public:
    AsiClient(QWidget *parent = nullptr);
    ~AsiClient();
    void start();

private:
    Ui::AsiClient *ui;

    void loadConfiguration();
    void saveConfiguration();
    void createMenus();

    AsiReader *spectrograph;
    AsiReader *field;
    AsiReader *dome;

    QJsonObject configuration;

    AdjustmentPanel *adjustmentPanel;
    AutoGuidePanel *autoGuidePanel;
    LogViewer *logsPanel;

    AsiReader::AsiParams *spectrograph_params;
    AsiReader::AsiParams *field_params;
    AsiReader::AsiParams *dome_params;

    EthRlyReader *relayBoardReader;
    EthRlySetter *relayBoardSetLampOn;
    EthRlySetter *relayBoardSetLampOff;

    struct CrosshairParams {
        quint32 x;
        quint32 y;
        quint32 hole_width;
        CrosshairParams() {
            x = 0;
            y = 0;
            hole_width = 15;
        }
    };

    struct GridParams {
        quint32 x_center;
        quint32 y_center;
        quint32 h_spacing;
        quint32 v_spacing;
        GridParams() {
            x_center = 640;
            y_center = 480;
            h_spacing = 195;
            h_spacing = 195;
        }
    };

    CrosshairParams crosshair1_params;
    CrosshairParams crosshair2_params;

    GridParams fieldImageGridParams;

    QPixmap lastSpectrographImage;
    QPixmap lastFieldImage;
    QPixmap lastDomeImage;

    int spectrographFlipHorizontal;
    int spectrographFlipVertical;

    int fieldFlipHorizontal;
    int fieldFlipVertical;

    QImage setImageLevels(QImage image, quint32 black, quint32 range);

protected:
    void closeEvent(QCloseEvent *event);

public slots:
    // slider slots
    void getSpectrographExpTime(int value);
    void getSpectrographGain(int value);

    void getFieldExpTime(int value);
    void getFieldGain(int value);

    void getDomeExpTime(int value);
    void getDomeGain(int value);

    void updateSpectrographFlipHorizontal(int state);
    void updateSpectrographFlipVertical(int state);
    void updateFieldFlipHorizontal(int state);
    void updateFieldFlipVertical(int state);

private slots:

    void configurationUpdated();

    // image slots
    void getSpectrographImage(QByteArray data);
    void getFieldImage(QByteArray data);
    void getDomeImage(QByteArray data);

    void updateCrosshair1Position();
    void updateCrosshair2Position();

    void updateFieldImageGrid();

    void updateLampStatus(EthRly::BoardStatus boardStatus);
    void setLampRelayOn();
    void setLampRelayOff();

    void updateSpectrographMouseCoords(QPointF point);

    void resetSpectrographImageLevels();

    void updateSpectrographImageRange(int value);

    // menu slots
    void saveSpectrographImage();
    void saveFieldImage();
    void saveDomeImage();

    void openAdjustmentPanel();
    void openAutoGuidePanel();
    void openLogsPanel();

signals:
    void exited();
};
#endif // ASICLIENT_H
