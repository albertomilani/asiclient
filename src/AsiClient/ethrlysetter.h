#ifndef ETHRLYSETTER_H
#define ETHRLYSETTER_H

#include <QThread>
#include "ethrly.h"

class EthRlySetter : public QThread
{
    Q_OBJECT
public:
    EthRlySetter(QString board_address, quint16 board_port, quint8 requested_relay, bool requested_status);

    EthRly board;

protected:
    void run();

private:
    quint8 relay_num;
    bool relay_status;
};

#endif // ETHRLYSETTER_H
