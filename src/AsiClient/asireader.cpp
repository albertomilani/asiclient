#include "asireader.h"
#include <QImage>

AsiReader::AsiReader(QString address, quint16 port, AsiParams *parameters) : QThread()
{
    _address = address;
    _port = port;
    params = parameters;

    socket = NULL;
}

void AsiReader::run() {

    const int connection_timeout = 5 * 1000;
    const int read_timeout = 20 * 1000;

    if (socket == NULL) {
        socket = new QTcpSocket();
    }

    while (true) {

        socket->connectToHost(_address, _port);
        if (!socket->waitForConnected(connection_timeout)) {
            // emit error(socket->error(), socket->errorString());
        } else {

            char write_data[100];
            sprintf(write_data, "{\"gain\":%d,\"exp_time\":%d}", params->gain, (params->exp_time  * 1000));
            socket->write(write_data, qstrlen(write_data));

            while (socket->waitForReadyRead(read_timeout)) {
                QByteArray in_data;
                in_data = socket->readAll();
                data = data + in_data;

            }
            emit imageReceived(data);
            data = QByteArray();
        }
        socket->disconnectFromHost();
        QThread::msleep(100);
    }
}
