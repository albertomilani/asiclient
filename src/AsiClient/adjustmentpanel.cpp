#include "adjustmentpanel.h"
#include "ui_adjustmentpanel.h"
#include "config_keys.h"
#include <QJsonObject>

AdjustmentPanel::AdjustmentPanel(AsiClient *asi_client, QJsonObject *app_configuration, QWidget *parent) :
    QWidget(parent)
    , ui(new Ui::AdjustmentPanel)
    , configuration(app_configuration)
{
    ui->setupUi(this);

    // spectrograph EXP
    ui->vs_spectrographExp->setMinimum(20);
    ui->vs_spectrographExp->setMaximum(15000);
    ui->vs_spectrographExp->setSingleStep(10);
    ui->vs_spectrographExp->setPageStep(10);

    ui->sb_spectrographExp->setMinimum(20);
    ui->sb_spectrographExp->setMaximum(15000);
    ui->sb_spectrographExp->setValue(ui->vs_spectrographExp->value());

    // spectrograph GAIN
    ui->vs_spectrographGain->setMinimum(0);
    ui->vs_spectrographGain->setMaximum(300);
    ui->vs_spectrographGain->setSingleStep(1);
    ui->vs_spectrographGain->setPageStep(1);
    ui->vs_spectrographGain->setSliderPosition(150);

    ui->sb_spectrographGain->setMinimum(0);
    ui->sb_spectrographGain->setMaximum(300);
    ui->sb_spectrographGain->setValue(ui->vs_spectrographGain->value());

    // field EXP
    ui->vs_fieldExp->setMinimum(20);
    ui->vs_fieldExp->setMaximum(15000);
    ui->vs_fieldExp->setSingleStep(10);
    ui->vs_fieldExp->setPageStep(10);

    ui->sb_fieldExp->setMinimum(20);
    ui->sb_fieldExp->setMaximum(15000);
    ui->sb_fieldExp->setValue(ui->vs_fieldExp->value());

    // field GAIN
    ui->vs_fieldGain->setMinimum(0);
    ui->vs_fieldGain->setMaximum(300);
    ui->vs_fieldGain->setSingleStep(1);
    ui->vs_fieldGain->setPageStep(1);
    ui->vs_fieldGain->setSliderPosition(150);

    ui->sb_fieldGain->setMinimum(0);
    ui->sb_fieldGain->setMaximum(300);
    ui->sb_fieldGain->setValue(ui->vs_fieldGain->value());

    // dome EXP
    ui->vs_domeExp->setMinimum(20);
    ui->vs_domeExp->setMaximum(15000);
    ui->vs_domeExp->setSingleStep(10);
    ui->vs_domeExp->setPageStep(10);

    ui->sb_domeExp->setMinimum(20);
    ui->sb_domeExp->setMaximum(15000);
    ui->sb_domeExp->setValue(ui->vs_domeExp->value());

    // dome GAIN
    ui->vs_domeGain->setMinimum(0);
    ui->vs_domeGain->setMaximum(300);
    ui->vs_domeGain->setSingleStep(1);
    ui->vs_domeGain->setPageStep(1);
    ui->vs_domeGain->setSliderPosition(150);

    ui->sb_domeGain->setMinimum(0);
    ui->sb_domeGain->setMaximum(300);
    ui->sb_domeGain->setValue(ui->vs_domeGain->value());

    if (configuration->value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP) != QJsonValue::Null) {
        QJsonObject spectrographFlip_config = configuration->value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP).toObject();
        if (spectrographFlip_config.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_HORIZONTAL).toInt() > 0) {
            ui->cb_spectrographFlipHorizontal->setChecked(true);
        }
        if (spectrographFlip_config.value(CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_VERTICAL).toInt() > 0) {
            ui->cb_spectrographFlipVertical->setChecked(true);
        }
    }

    if (configuration->value(CONFIG_KEY_FIELD_IMAGE_FLIP) != QJsonValue::Null) {
        QJsonObject fieldFlip_config = configuration->value(CONFIG_KEY_FIELD_IMAGE_FLIP).toObject();
        if (fieldFlip_config.value(CONFIG_KEY_FIELD_IMAGE_FLIP_HORIZONTAL).toInt() > 0) {
            ui->cb_fieldFlipHorizontal->setChecked(true);
        }
        if (fieldFlip_config.value(CONFIG_KEY_FIELD_IMAGE_FLIP_VERTICAL).toInt() > 0) {
            ui->cb_fieldFlipVertical->setChecked(true);
        }
    }

    // connect slider to asiclient spectrograph slots
    connect(ui->vs_spectrographExp, &QSlider::valueChanged, asi_client, &AsiClient::getSpectrographExpTime);
    connect(ui->vs_spectrographGain, &QSlider::valueChanged, asi_client, &AsiClient::getSpectrographGain);

    // connect slider to spinbox
    connect(ui->vs_spectrographExp, SIGNAL(valueChanged(int)), ui->sb_spectrographExp, SLOT(setValue(int)));
    connect(ui->vs_spectrographGain, SIGNAL(valueChanged(int)), ui->sb_spectrographGain, SLOT(setValue(int)));

    // connect spinbox to slider
    connect(ui->sb_spectrographExp, SIGNAL(valueChanged(int)), ui->vs_spectrographExp, SLOT(setValue(int)));
    connect(ui->sb_spectrographGain, SIGNAL(valueChanged(int)), ui->vs_spectrographGain, SLOT(setValue(int)));

    // connect slider to asiclient field slots
    connect(ui->vs_fieldExp, &QSlider::valueChanged, asi_client, &AsiClient::getFieldExpTime);
    connect(ui->vs_fieldGain, &QSlider::valueChanged, asi_client, &AsiClient::getFieldGain);

    // connect slider to spinbox
    connect(ui->vs_fieldExp, SIGNAL(valueChanged(int)), ui->sb_fieldExp, SLOT(setValue(int)));
    connect(ui->vs_fieldGain, SIGNAL(valueChanged(int)), ui->sb_fieldGain, SLOT(setValue(int)));

    // connect spinbox to slider
    connect(ui->sb_fieldExp, SIGNAL(valueChanged(int)), ui->vs_fieldExp, SLOT(setValue(int)));
    connect(ui->sb_fieldGain, SIGNAL(valueChanged(int)), ui->vs_fieldGain, SLOT(setValue(int)));

    // connect slider to asiclient dome slots
    connect(ui->vs_domeExp, &QSlider::valueChanged, asi_client, &AsiClient::getDomeExpTime);
    connect(ui->vs_domeGain, &QSlider::valueChanged, asi_client, &AsiClient::getDomeGain);

    // connect slider to spinbox
    connect(ui->vs_domeExp, SIGNAL(valueChanged(int)), ui->sb_domeExp, SLOT(setValue(int)));
    connect(ui->vs_domeGain, SIGNAL(valueChanged(int)), ui->sb_domeGain, SLOT(setValue(int)));

    // connect spinbox to slider
    connect(ui->sb_domeExp, SIGNAL(valueChanged(int)), ui->vs_domeExp, SLOT(setValue(int)));
    connect(ui->sb_domeGain, SIGNAL(valueChanged(int)), ui->vs_domeGain, SLOT(setValue(int)));

    // connect spectrograph image flip checkboxes
    connect(ui->cb_spectrographFlipHorizontal, &QCheckBox::stateChanged, asi_client, &AsiClient::updateSpectrographFlipHorizontal);
    connect(ui->cb_spectrographFlipVertical, &QCheckBox::stateChanged, asi_client, &AsiClient::updateSpectrographFlipVertical);

    // connect field image flip checkboxes
    connect(ui->cb_fieldFlipHorizontal, &QCheckBox::stateChanged, asi_client, &AsiClient::updateFieldFlipHorizontal);
    connect(ui->cb_fieldFlipVertical, &QCheckBox::stateChanged, asi_client, &AsiClient::updateFieldFlipVertical);
}

AdjustmentPanel::~AdjustmentPanel()
{
    delete ui;
}
