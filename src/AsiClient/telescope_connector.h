#ifndef TELESCOPERECEIVER_H
#define TELESCOPERECEIVER_H

#include <QThread>
#include <QTcpSocket>
#include <QQueue>

#include "telescope_message.h"
#include "logviewer.h"

class TelescopeConnector : public QThread
{
    Q_OBJECT

public:
    TelescopeConnector(QString addres, quint16 port, LogViewer *mainLogger);

    void newMessage(TelescopeNetMessage *message);

    void requestDisconnection();

protected:

    LogViewer *logger;

    QString telescopeAddress;
    quint16 telescopePort;

    QTcpSocket *telescopeSocket;

    bool requestedDisconnection;

    QQueue<TelescopeNetMessage*> messageQueue;

    void run();

signals:
    void telescopeConnected();
    void telescopeMessageAvailable(QByteArray msg);
};

#endif // TELESCOPERECEIVER_H
