#ifndef AUTOGUIDEPANEL_H
#define AUTOGUIDEPANEL_H

#include <QWidget>
#include <QLineSeries>
#include "asireader.h"
#include "config_keys.h"
#include "telescope.h"
#include "logviewer.h"

using namespace QtCharts;

namespace Ui {
class AutoGuidePanel;
}

class AutoGuidePanel : public QWidget
{
    Q_OBJECT

public:
    explicit AutoGuidePanel(AsiReader *image_reader, QJsonObject *app_configuration, LogViewer *mainLogger, QWidget *parent = nullptr);
    ~AutoGuidePanel();

    quint32 getBoxXCenter();
    quint32 getBoxYCenter();
    quint32 getBoxInnerSize();
    quint32 getBoxOuterSize();

    void setSpectrographFlipHorizontal(int value);
    void setSpectrographFlipVertical(int value);

private:
    Ui::AutoGuidePanel *ui;

    LogViewer *logger;

    struct GuideBoxParams {
        quint32 xCenter;
        quint32 yCenter;
        quint32 innerSize;
        quint32 outerSize;

        GuideBoxParams() {
            xCenter = 640;
            yCenter = 480;
            innerSize = 30;
            outerSize = 40;
        }
    };

    GuideBoxParams guideBox;
    AsiReader *imageReader;
    QJsonObject *configuration;

    QLineSeries *backgroundValuesSeries;
    QChart *backgroundValuesChart;
    qint32 backgroundValuesChartTimeRange;

    qreal backgroundValuesYmin;
    qreal backgroundValuesYmax;

    QLineSeries *starCenterPositionXSeries;
    QLineSeries *starCenterPositionYSeries;
    QChart *starCenterPositionChart;
    qint32 starCenterPositionChartTimeRange;

    qreal starCenterPositionChartYMin;
    qreal starCenterPositionChartYMax;

    struct TelescopeSettings {
        qreal northStep;
        qreal southStep;
        qreal eastStep;
        qreal westStep;

        qreal northBackslash;
        qreal southBackslash;
        qreal eastBackslash;
        qreal westBackslash;

        QString serverAddress;
        quint16 serverPort;

        TelescopeSettings() {
            northStep = 0;
            southStep = 0;
            eastStep = 0;
            westStep = 0;

            northBackslash = 0;
            southBackslash = 0;
            eastBackslash = 0;
            westBackslash = 0;
        }
    };

    TelescopeSettings telescopeParams;

    struct GuideSettings {
        qreal arcsecPerPixelDec;
        qreal arcsecPerPixelRa;
        qreal starGogPower;
        qreal minimumSecondsIntervalDec;
        qreal minimumSecondsIntervalRa;
        qreal minimumArcsecShiftDec;
        qreal minimumArcsecShiftRa;

        bool mirror_ra;
        bool mirror_dec;
        bool axes_rotation;

        GuideSettings() {
            arcsecPerPixelDec = 0;
            arcsecPerPixelRa = 0;
            starGogPower = 1;
            minimumSecondsIntervalDec = 0;
            minimumSecondsIntervalRa = 0;
            minimumArcsecShiftDec = 0;
            minimumArcsecShiftRa = 0;
            mirror_ra = false;
            mirror_dec = false;
            axes_rotation = false;
        }
    };

    Telescope *telescopeClient;

    GuideSettings autoguideSettings;

    quint32 computeImageMedian(QImage image, quint64 rows, quint64 columns);
    quint32 computeBackgroundMedian(QImage image);
    QImage imageSubctratScalar(QImage image, quint32 value);
    QPointF computeStarCenter(QImage image);

    /**
     * @brief formatRa
     * @param ra Right ascension in radians
     * @return QString Format 00h 00m 00s
     */
    QString formatRa(double ra);

    /**
     * @brief formatDec
     * @param dec Declination in radians
     * @return QString Format 00° 00' 00"
     */
    QString formatDec(double dec);

    /**
     * @brief moveTelescope
     * @param ra radians
     * @param dec radians
     */
    void moveTelescope(double ra, double dec);

    /**
     * @brief guideMoveTelescope
     * @param raPixelShift
     * @param decPixelShift
     */
    void guideMoveTelescope(double raPixelShift, double decPixelShift);

    Telescope::RaDecCoords telescopePosition;

    qint64 lastAutoguideCorrectionRa;
    qint64 lastAutoguideCorrectionDec;

    enum TelescopeDirection {
        NONE,
        NORTH,
        SOUTH,
        EAST,
        WEST
    };

    TelescopeDirection lastTelescopeMoveDirection;

    /**
     * @brief autoguideStatus true: guiding, false: not guiding
     */
    bool autoguideEnabled;

    int spectrographFlipHorizontal;
    int spectrographFlipVertical;

private slots:
    void getImage(QByteArray data);

    void updateGuideBoxParams();
    void updateBackgroundValuesChart(int index);
    void updateStarCenterPositionChart(int index);
    void updateTelescopeMoveStep();
    void updateTelescopeBackslash();
    void updateGuideSettingsArcsecPixel();
    void updateGuideSettingsMinimumInterval();
    void updateGuideSettingsMinimumShift();
    void updateGuideSettingsAxes();
    void updateGuideSettingsStarCogPower();

    void updateBackgroundValuesTimeRange(int value);
    void updateStarCenterPositionTimeRange(int value);

    void connectTelescope();
    void disconnectTelescope();

    void updateTelescopeConnectionStatus();

    void getTelescopePosition();

    void moveTelescopeNorth();
    void moveTelescopeSouth();
    void moveTelescopeEast();
    void moveTelescopeWest();

    void updateTelescopePosition(Telescope::RaDecCoords coords);

    void startAutoguide();
    void stopAutoguide();

signals:
    void configurationChanged();

};

#endif // AUTOGUIDEPANEL_H
