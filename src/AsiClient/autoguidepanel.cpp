#include "autoguidepanel.h"
#include "ui_autoguidepanel.h"
#include <QJsonObject>
#include <QtCharts>
#include <QtMath>

using namespace QtCharts;

AutoGuidePanel::AutoGuidePanel(AsiReader *image_reader, QJsonObject *app_configuration, LogViewer *mainLogger, QWidget *parent) :
  QWidget(parent)
  , ui(new Ui::AutoGuidePanel)
  , logger(mainLogger)
  , guideBox()
  , imageReader(image_reader)
  , configuration(app_configuration)
  , backgroundValuesSeries(NULL)
  , backgroundValuesChart(NULL)
  , backgroundValuesChartTimeRange(60)
  , backgroundValuesYmin(0)
  , backgroundValuesYmax(1)
  , starCenterPositionXSeries(NULL)
  , starCenterPositionYSeries(NULL)
  , starCenterPositionChart(NULL)
  , starCenterPositionChartTimeRange(60)
  , starCenterPositionChartYMin(-1)
  , starCenterPositionChartYMax(1)
  , telescopeParams()
  , telescopeClient(NULL)
  , autoguideSettings()
  , lastAutoguideCorrectionRa(0)
  , lastAutoguideCorrectionDec(0)
  , lastTelescopeMoveDirection(TelescopeDirection::NONE)
  , autoguideEnabled(false)
  , spectrographFlipHorizontal(0)
  , spectrographFlipVertical(0)
{
    ui->setupUi(this);

    ui->cb_guidePreviewToggleBackground->setChecked(true);
    ui->cb_guidePreviewToggleInnerBox->setChecked(true);
    ui->cb_guidePreviewToggleStarCenter->setChecked(true);

    if (configuration->value(CONFIG_KEY_AUTOGUIDE) != QJsonValue::Null) {

        QJsonObject autoguideConfig = configuration->value(CONFIG_KEY_AUTOGUIDE).toObject();

        guideBox.xCenter   = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_X_CENTER).toInt();
        guideBox.yCenter   = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_Y_CENTER).toInt();
        guideBox.innerSize = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_INNER_BOX_CENTER).toInt();
        guideBox.outerSize = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_OUTER_BOX_CENTER).toInt();

        ui->le_xCenter->setText(QString::number(guideBox.xCenter));
        ui->le_yCenter->setText(QString::number(guideBox.yCenter));
        ui->le_innerBox->setText(QString::number(guideBox.innerSize));
        ui->le_outerBox->setText(QString::number(guideBox.outerSize));

        autoguideSettings.arcsecPerPixelDec         = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_ARCSEC_PER_PIXEL_DEC).toDouble();
        autoguideSettings.arcsecPerPixelRa          = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_ARCSEC_PER_PIXEL_RA).toDouble();
        autoguideSettings.minimumSecondsIntervalDec = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_MINIMUM_SECONDS_INTERVAL_DEC).toDouble();
        autoguideSettings.minimumSecondsIntervalRa  = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_MINIMUM_SECONDS_INTERVAL_RA).toDouble();
        autoguideSettings.minimumArcsecShiftDec     = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_MINIMUM_ARCSEC_SHIFT_DEC).toDouble();
        autoguideSettings.minimumArcsecShiftRa      = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_MINIMUM_ARCSEC_SHIFT_RA).toDouble();
        autoguideSettings.mirror_ra                 = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_MIRROR_RA).toBool();
        autoguideSettings.mirror_dec                = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_MIRROR_DEC).toBool();
        autoguideSettings.axes_rotation             = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_AXES_ROTATION).toBool();
        autoguideSettings.starGogPower              = autoguideConfig.value(CONFIG_KEY_AUTOGUIDE_STAR_COG_POWER).toDouble();

        ui->le_arcsecPixelDec->setText(QString::number(autoguideSettings.arcsecPerPixelDec));
        ui->le_arcsecPixelRa->setText(QString::number(autoguideSettings.arcsecPerPixelRa));

        ui->le_minimumIntervalDec->setText(QString::number(autoguideSettings.minimumSecondsIntervalDec));
        ui->le_minimumIntervalRa->setText(QString::number(autoguideSettings.minimumSecondsIntervalRa));

        ui->le_minimumArcsecShiftRa->setText(QString::number(autoguideSettings.minimumArcsecShiftRa));
        ui->le_minimumArcsecShiftDec->setText(QString::number(autoguideSettings.minimumArcsecShiftDec));

        ui->cb_mirrorRA->setChecked(autoguideSettings.mirror_ra);
        ui->cb_mirrorDec->setChecked(autoguideSettings.mirror_dec);
        ui->cb_axesRotation->setChecked(autoguideSettings.axes_rotation);

        ui->sb_starCogPower->setValue(autoguideSettings.starGogPower);

        connect(ui->cb_mirrorRA, &QCheckBox::stateChanged, this, &AutoGuidePanel::updateGuideSettingsAxes);
        connect(ui->cb_mirrorDec, &QCheckBox::stateChanged, this, &AutoGuidePanel::updateGuideSettingsAxes);
        connect(ui->cb_axesRotation, &QCheckBox::stateChanged, this, &AutoGuidePanel::updateGuideSettingsAxes);

        connect(ui->pb_setGuideSettingsArcsecPixel, &QPushButton::clicked, this, &AutoGuidePanel::updateGuideSettingsArcsecPixel);
        connect(ui->pb_setGuideSettingsMinimumInterval, &QPushButton::clicked, this, &AutoGuidePanel::updateGuideSettingsMinimumInterval);
        connect(ui->pb_setGuideSettingsMinimumArcsecShift, &QPushButton::clicked, this, &AutoGuidePanel::updateGuideSettingsMinimumShift);

        connect(ui->pb_setGuideSettingsStarCogPower, &QPushButton::clicked, this, &AutoGuidePanel::updateGuideSettingsStarCogPower);
    }

    if (configuration->value(CONFIG_KEY_TELESCOPE_SETTINGS) != QJsonValue::Null) {

        QJsonObject telescopeConfig = configuration->value(CONFIG_KEY_TELESCOPE_SETTINGS).toObject();

        if (telescopeConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP) != QJsonValue::Null) {

            QJsonObject moveStepConfig = telescopeConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP).toObject();

            telescopeParams.northStep = moveStepConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_NORTH).toDouble();
            telescopeParams.southStep = moveStepConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_SOUTH).toDouble();
            telescopeParams.eastStep  = moveStepConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_EAST).toDouble();
            telescopeParams.westStep  = moveStepConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_WEST).toDouble();

            ui->le_northSteps->setText(QString::number(telescopeParams.northStep));
            ui->le_southSteps->setText(QString::number(telescopeParams.southStep));
            ui->le_eastSteps->setText(QString::number(telescopeParams.eastStep));
            ui->le_westSteps->setText(QString::number(telescopeParams.westStep));

            connect(ui->pb_setMoveSteps, &QPushButton::clicked, this, &AutoGuidePanel::updateTelescopeMoveStep);
            connect(ui->pb_setMoveBackslash, &QPushButton::clicked, this, &AutoGuidePanel::updateTelescopeBackslash);
        }

        if (telescopeConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH) != QJsonValue::Null) {

            QJsonObject backslashConfig = telescopeConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH).toObject();

            telescopeParams.northBackslash = backslashConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_NORTH).toDouble();
            telescopeParams.southBackslash = backslashConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_SOUTH).toDouble();
            telescopeParams.eastBackslash  = backslashConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_EAST).toDouble();
            telescopeParams.westBackslash  = backslashConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_WEST).toDouble();

            ui->le_northBackslash->setText(QString::number(telescopeParams.northBackslash));
            ui->le_southBackslash->setText(QString::number(telescopeParams.southBackslash));
            ui->le_eastBackslash->setText(QString::number(telescopeParams.eastBackslash));
            ui->le_westBackslash->setText(QString::number(telescopeParams.westBackslash));
        }

        if (telescopeConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_SERVER) != QJsonValue::Null) {

            QJsonObject serverConfig = telescopeConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_SERVER).toObject();

            telescopeParams.serverAddress = serverConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_SERVER_ADDRESS).toString();
            telescopeParams.serverPort    = (quint16)serverConfig.value(CONFIG_KEY_TELESCOPE_SETTINGS_SERVER_PORT).toInt();

            // connect/disconnect buttons
            connect(ui->pb_connectTelescope, &QPushButton::clicked, this, &AutoGuidePanel::connectTelescope);
            connect(ui->pb_disconnectTelescope, &QPushButton::clicked, this, &AutoGuidePanel::disconnectTelescope);

            // get position button
            connect(ui->pb_telescopePosition, &QPushButton::clicked, this, &AutoGuidePanel::getTelescopePosition);

            // move buttons
            connect(ui->pb_moveTelescopeNorth, &QPushButton::clicked, this, &AutoGuidePanel::moveTelescopeNorth);
            connect(ui->pb_moveTelescopeSouth, &QPushButton::clicked, this, &AutoGuidePanel::moveTelescopeSouth);
            connect(ui->pb_moveTelescopeEast, &QPushButton::clicked, this, &AutoGuidePanel::moveTelescopeEast);
            connect(ui->pb_moveTelescopeWest, &QPushButton::clicked, this, &AutoGuidePanel::moveTelescopeWest);

            // start/stop autoguide buttons
            stopAutoguide();
            ui->pb_startAutoguide->setDisabled(true);
            ui->pb_stopAutoguide->setDisabled(true);
            connect(ui->pb_startAutoguide, &QPushButton::clicked, this, &AutoGuidePanel::startAutoguide);
            connect(ui->pb_stopAutoguide, &QPushButton::clicked, this, &AutoGuidePanel::stopAutoguide);


        } else {

            ui->pb_connectTelescope->setDisabled(true);
            ui->pb_disconnectTelescope->setDisabled(true);
        }

    }

    connect(imageReader, &AsiReader::imageReceived, this, &AutoGuidePanel::getImage);

    connect(ui->pb_setGuideBoxData, &QPushButton::released, this, &AutoGuidePanel::updateGuideBoxParams);

    // backgraound level plot
    backgroundValuesSeries = new QLineSeries();

    backgroundValuesChart = new QChart();
    backgroundValuesChart->legend()->hide();
    backgroundValuesChart->addSeries(backgroundValuesSeries);
    backgroundValuesChart->setTitle("Background level median");

    QDateTimeAxis *backgroundValuesChartAxisX = new QDateTimeAxis;
    backgroundValuesChartAxisX->setFormat("hh:mm:ss");
    backgroundValuesChartAxisX->setTitleText("Time");
    backgroundValuesChart->addAxis(backgroundValuesChartAxisX, Qt::AlignBottom);
    backgroundValuesSeries->attachAxis(backgroundValuesChartAxisX);

    QValueAxis *backgroundValuesChartAxisY = new QValueAxis;
    backgroundValuesChartAxisY->setLabelFormat("%i");
    backgroundValuesChartAxisY->setTitleText("ADU");
    backgroundValuesChart->addAxis(backgroundValuesChartAxisY, Qt::AlignLeft);
    backgroundValuesSeries->attachAxis(backgroundValuesChartAxisY);

    ui->cv_levelsPlot->setChart(backgroundValuesChart);
    ui->sb_backgroundValuesTimeRange->setValue(backgroundValuesChartTimeRange);

    connect(backgroundValuesSeries, &QLineSeries::pointAdded, this, &AutoGuidePanel::updateBackgroundValuesChart);
    connect(ui->sb_backgroundValuesTimeRange, SIGNAL(valueChanged(int)), this, SLOT(updateBackgroundValuesTimeRange(int)));

    // star center position plot
    starCenterPositionXSeries = new QLineSeries();
    starCenterPositionYSeries = new QLineSeries();

    starCenterPositionChart = new QChart();
    starCenterPositionChart->legend()->hide();
    starCenterPositionChart->addSeries(starCenterPositionXSeries);
    starCenterPositionChart->addSeries(starCenterPositionYSeries);
    starCenterPositionChart->setTitle("Star center position residuals");

    QDateTimeAxis *starCenterPositionChartAxisX = new QDateTimeAxis;
    starCenterPositionChartAxisX->setFormat("hh:mm:ss");
    starCenterPositionChartAxisX->setTitleText("Time");
    starCenterPositionChart->addAxis(starCenterPositionChartAxisX, Qt::AlignBottom);
    starCenterPositionXSeries->attachAxis(starCenterPositionChartAxisX);
    starCenterPositionYSeries->attachAxis(starCenterPositionChartAxisX);

    QValueAxis *starCenterPositionChartAxisY = new QValueAxis;
    starCenterPositionChartAxisY->setLabelFormat("%.1f");
    starCenterPositionChartAxisY->setTitleText("Pixels");
    starCenterPositionChart->addAxis(starCenterPositionChartAxisY, Qt::AlignLeft);
    starCenterPositionXSeries->attachAxis(starCenterPositionChartAxisY);
    starCenterPositionYSeries->attachAxis(starCenterPositionChartAxisY);

    ui->cv_positionErrorsPlot->setChart(starCenterPositionChart);
    ui->sb_starCenterPositionTimeRange->setValue(starCenterPositionChartTimeRange);

    connect(starCenterPositionXSeries, &QLineSeries::pointAdded, this, &AutoGuidePanel::updateStarCenterPositionChart);
    connect(starCenterPositionYSeries, &QLineSeries::pointAdded, this, &AutoGuidePanel::updateStarCenterPositionChart);
    connect(ui->sb_starCenterPositionTimeRange, SIGNAL(valueChanged(int)), this, SLOT(updateStarCenterPositionTimeRange(int)));

}

AutoGuidePanel::~AutoGuidePanel()
{
    delete ui;
}

void AutoGuidePanel::getImage(QByteArray data) {

    qreal previewSize = 150;

    if (isVisible()) {
        QImage srcImg = QImage((const uchar*)data.data(), 1280, 960, QImage::Format_Grayscale8);

        QTransform flipTransformation;

        if (spectrographFlipHorizontal != 0) {
            flipTransformation.scale(1,-1);
        }
        if (spectrographFlipVertical != 0) {
            flipTransformation.scale(-1,1);
        }

        QPixmap flippedImage = QPixmap::fromImage(srcImg).transformed(flipTransformation);
        QImage img = flippedImage.toImage();

        // crop image
        QRect rect(guideBox.xCenter - guideBox.outerSize/2, guideBox.yCenter - guideBox.outerSize/2, guideBox.outerSize, guideBox.outerSize);
        QImage outerBoxImage = img.copy(rect);

        // compute background median
        quint32 median = computeBackgroundMedian(outerBoxImage);

        // subtract background level
        QImage outerBoxImageNoBackground = imageSubctratScalar(outerBoxImage, median);

        // crop inner box region
        quint32 innerRectCorner = (qreal)(guideBox.outerSize - guideBox.innerSize) / 2.0;
        QRect innerRect(innerRectCorner, innerRectCorner, guideBox.innerSize, guideBox.innerSize);
        QImage innerBoxImage = outerBoxImage.copy(innerRect);

        // compute star center
        QPointF starCenter = computeStarCenter(innerBoxImage);

        // move telescope
        QPointF shift = QPointF(guideBox.innerSize/2, guideBox.innerSize/2) - starCenter;
        guideMoveTelescope(shift.x(), shift.y());

        // scale and rotate image just before drawing
        QPixmap previewScaled;
        if (ui->cb_guidePreviewToggleBackground->isChecked()) {
            previewScaled = QPixmap::fromImage(outerBoxImageNoBackground);
        } else {
            previewScaled = QPixmap::fromImage(outerBoxImage);
        }
        previewScaled = previewScaled.scaled(previewSize, previewSize);

        QPainter autoguideBoxes(&previewScaled);

        double boxDiffSize = (qreal)(guideBox.outerSize - guideBox.innerSize) / 2.0;

        // draw star center
        if (ui->cb_guidePreviewToggleStarCenter->isChecked()) {

            QPointF drawStarCenter = (starCenter + QPointF(boxDiffSize, boxDiffSize)) * (previewSize/(qreal)guideBox.outerSize) ;
            autoguideBoxes.setPen("#ff0000");
            autoguideBoxes.drawLine(drawStarCenter.x() - 5, drawStarCenter.y(), drawStarCenter.x() + 5, drawStarCenter.y());
            autoguideBoxes.drawLine(drawStarCenter.x(), drawStarCenter.y() - 5, drawStarCenter.x(), drawStarCenter.y() + 5);
        }

        // draw inner box
        if (ui->cb_guidePreviewToggleInnerBox->isChecked()) {

            autoguideBoxes.setPen("#66cc66");
            quint32 rectCorner = (qreal)(guideBox.outerSize - guideBox.innerSize) / 2.0 / (qreal)guideBox.outerSize * previewSize;
            quint32 rectSize = (qreal)guideBox.innerSize / (qreal)guideBox.outerSize * previewSize;
            autoguideBoxes.drawRect(rectCorner, rectCorner, rectSize, rectSize);
        }

        // draw ra/dec axes
        if (ui->cb_guidePreviewToggleShowAxes->isChecked()) {

            autoguideBoxes.setPen("#0066cc");

            autoguideBoxes.drawLine(10, 20, 60, 20);
            autoguideBoxes.drawLine(20, 10, 20, 60);
            autoguideBoxes.drawText(10, 68, "R");
            autoguideBoxes.drawText(68, 20, "D");

        }

        QTransform transformation;
        transformation.rotate(90);

        QPixmap previewRotated = previewScaled.transformed(transformation);

        // update preview
        ui->lb_guideImagePreview->setPixmap(previewRotated);
        ui->lb_guideImagePreview->update();

        // get actual time
        QDateTime now = QDateTime::currentDateTime();

        // create background plot point
        backgroundValuesChart->axes(Qt::Horizontal).back()->setMin(now.addSecs(-backgroundValuesChartTimeRange));
        backgroundValuesChart->axes(Qt::Horizontal).back()->setMax(now.addSecs(0));
        backgroundValuesSeries->append(now.toMSecsSinceEpoch(), median);

        // create background plot point
        starCenterPositionChart->axes(Qt::Horizontal).back()->setMin(now.addSecs(-starCenterPositionChartTimeRange));
        starCenterPositionChart->axes(Qt::Horizontal).back()->setMax(now.addSecs(0));
        starCenterPositionXSeries->append(now.toMSecsSinceEpoch(), starCenter.x() - guideBox.innerSize/2.0);
        starCenterPositionYSeries->append(now.toMSecsSinceEpoch(), starCenter.y() - guideBox.innerSize/2.0);
    }

}

QImage AutoGuidePanel::imageSubctratScalar(QImage image, quint32 value) {

    QColor color;
    quint32 level;
    for (int i=0; i<image.width(); i++) {
        for(int j=0; j<image.height(); j++) {
            color = image.pixel(i, j);
            if (color.red() < (int)value) {
                level = 0;
            } else {
                level = color.red() - value;
            }
            image.setPixel(i, j, qRgb(level, level, level));
        }
    }

    return image;
}

quint32 AutoGuidePanel::computeImageMedian(QImage image, quint64 rows, quint64 columns) {

    quint32 median = 0;
    std::vector<quint32> data;
    QColor color;

    for (quint32 i=0; i<rows; i++) {
        quint32 *ptr = reinterpret_cast<quint32*>(image.scanLine(i));
        for (quint32 j=0; j<columns; j++) {
            color = ptr[j];
            data.push_back((color.red()));
        }
    }

    std::sort(data.begin(), data.end());

    if (data.size() % 2 == 0) {
        median = (data[data.size()/2 - 1] + data[data.size()/2]) / 2;
    } else {
        median = data[data.size()/2];
    }

    return median;
}

quint32 AutoGuidePanel::computeBackgroundMedian(QImage image) {

    quint32 median = 0;
    quint32 delta = (qreal)(guideBox.outerSize - guideBox.innerSize) / 2.0;
    QColor color;

    std::vector<quint32> data;
    for (quint32 i=0; i<guideBox.outerSize; i++) {
        quint32 *ptr = reinterpret_cast<quint32*>(image.scanLine(i));
        /*
        ########
        ########
        **    **
        **    **
        **    **
        **    **
        ********
        ********
        */
        if (i < delta) {
            for (quint32 j=0; j<guideBox.outerSize; j++) {
                color = ptr[j];
                data.push_back(color.red());
            }
        }
        /*
        ********
        ********
        ##    ##
        ##    ##
        ##    ##
        ##    ##
        ********
        ********
        */
        if (i >= delta && i < (image.height() - guideBox.innerSize)) {
            for (quint32 j=0; j<guideBox.outerSize; j++) {
                if (j < delta || j >= (image.width() - (guideBox.innerSize + delta))) {
                    color = ptr[j];
                    data.push_back(color.red());
                }
            }
        }
        /*
        ********
        ********
        **    **
        **    **
        **    **
        **    **
        ########
        ########
        */
        if (i >= (image.height() - (guideBox.innerSize + delta))) {
            for (quint32 j=0; j<guideBox.outerSize; j++) {
                color = ptr[j];
                data.push_back(color.red());
            }
        }
    }

    std::sort(data.begin(), data.end());

    if (data.size() % 2 == 0) {
        median = (data[data.size()/2 - 1] + data[data.size()/2]) / 2;
    } else {
        median = data[data.size()/2];
    }

    return median;
}

QPointF AutoGuidePanel::computeStarCenter(QImage image) {

    qreal x;
    qreal y;
    qreal xMean = 0;
    qreal yMean = 0;
    qreal xWeight = 0;
    qreal yWeight = 0;

    for (int i=0; i<image.width(); i++) {
        x = 0;
        for (int j=0; j<image.height(); j++) {
            x += pow((quint32)(image.pixelColor(i, j).lightness()), autoguideSettings.starGogPower);
        }
        x /= image.height();
        xMean = xMean + (x * (i + 1));
        xWeight += x;
    }

    for (int i=0; i<image.height(); i++) {
        y = 0;
        for (int j=0; j<image.width(); j++) {
            y += pow((quint32)(image.pixelColor(j, i).lightness()), autoguideSettings.starGogPower);
        }
        y /= image.width();
        yMean = yMean + (y * (i + 1));
        yWeight += y;
    }

    return QPointF((xMean/xWeight), (yMean/yWeight));
}

void AutoGuidePanel::updateBackgroundValuesChart(int index) {

    // delete old points
    QDateTime now = QDateTime::currentDateTime();
    now = now.addSecs(-backgroundValuesChartTimeRange);
    qint64 timestamp = now.toMSecsSinceEpoch();
    for (int i=0; i<backgroundValuesSeries->count(); i++) {
        if (backgroundValuesSeries->at(i).x() < timestamp) {
            backgroundValuesSeries->remove(i);
        } else {
            break;
        }
    }

    // find y max
    qreal y = backgroundValuesSeries->at(index).y();
    if (y > backgroundValuesYmax) {
        backgroundValuesChart->axes(Qt::Vertical).back()->setMax(y*1.1);
        backgroundValuesYmax = y;
    }

}

void AutoGuidePanel::updateStarCenterPositionChart(int index) {

    Q_UNUSED(index);

    // delete old points
    QDateTime now = QDateTime::currentDateTime();
    now = now.addSecs(-backgroundValuesChartTimeRange);
    qint64 timestamp = now.toMSecsSinceEpoch();
    for (int i=0; i<starCenterPositionXSeries->count(); i++) {
        if (starCenterPositionXSeries->at(i).x() < timestamp) {
            starCenterPositionXSeries->remove(i);
        } else {
            break;
        }
    }
    for (int i=0; i<starCenterPositionYSeries->count(); i++) {
        if (starCenterPositionYSeries->at(i).x() < timestamp) {
            starCenterPositionYSeries->remove(i);
        } else {
            break;
        }
    }

    // find y max and min
    int pointCount;
    qreal y;

    pointCount = starCenterPositionXSeries->count();
    y = starCenterPositionXSeries->at(pointCount-1).y();
    if (y > starCenterPositionChartYMax) {
        starCenterPositionChart->axes(Qt::Vertical).back()->setMax(y*1.1);
        starCenterPositionChartYMax = y;
    }
    if (y < starCenterPositionChartYMin) {
        starCenterPositionChart->axes(Qt::Vertical).back()->setMin(y - (qFabs(y)*0.1));
        starCenterPositionChartYMin = y;
    }

    pointCount = starCenterPositionYSeries->count();
    y = starCenterPositionYSeries->at(pointCount-1).y();
    if (y > starCenterPositionChartYMax) {
        starCenterPositionChart->axes(Qt::Vertical).back()->setMax(y*1.1);
        starCenterPositionChartYMax = y;
    }
    if (y < starCenterPositionChartYMin) {
        starCenterPositionChart->axes(Qt::Vertical).back()->setMin(y - (qFabs(y)*0.1));
        starCenterPositionChartYMin = y;
    }
}

void AutoGuidePanel::updateGuideBoxParams() {

    guideBox.xCenter = ui->le_xCenter->text().toUInt();
    guideBox.yCenter = ui->le_yCenter->text().toUInt();

    guideBox.innerSize = ui->le_innerBox->text().toUInt();
    guideBox.outerSize = ui->le_outerBox->text().toUInt();

    QJsonObject autoguide_config = configuration->value(CONFIG_KEY_AUTOGUIDE).toObject();

    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_X_CENTER, QJsonValue((int)guideBox.xCenter));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_Y_CENTER, QJsonValue((int)guideBox.yCenter));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_INNER_BOX_CENTER, QJsonValue((int)guideBox.innerSize));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_OUTER_BOX_CENTER, QJsonValue((int)guideBox.outerSize));

    configuration->insert(CONFIG_KEY_AUTOGUIDE, QJsonValue(autoguide_config));

    emit configurationChanged();
}

void AutoGuidePanel::updateTelescopeMoveStep() {

    QJsonObject telescope_config = configuration->value(CONFIG_KEY_TELESCOPE_SETTINGS).toObject();

    QJsonObject steps_config = telescope_config.value(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP).toObject();

    steps_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_EAST, QJsonValue((double)ui->le_eastSteps->text().toDouble()));
    steps_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_WEST, QJsonValue((double)ui->le_westSteps->text().toDouble()));
    steps_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_SOUTH, QJsonValue((double)ui->le_southSteps->text().toDouble()));
    steps_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_NORTH, QJsonValue((double)ui->le_northSteps->text().toDouble()));

    telescope_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP, QJsonValue(steps_config));

    configuration->insert(CONFIG_KEY_TELESCOPE_SETTINGS, QJsonValue(telescope_config));

    emit configurationChanged();

}

void AutoGuidePanel::updateTelescopeBackslash() {

    QJsonObject telescope_config = configuration->value(CONFIG_KEY_TELESCOPE_SETTINGS).toObject();

    QJsonObject backslash_config = telescope_config.value(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH).toObject();

    backslash_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_EAST, QJsonValue((double)ui->le_eastBackslash->text().toDouble()));
    backslash_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_WEST, QJsonValue((double)ui->le_westBackslash->text().toDouble()));
    backslash_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_SOUTH, QJsonValue((double)ui->le_southBackslash->text().toDouble()));
    backslash_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_NORTH, QJsonValue((double)ui->le_northBackslash->text().toDouble()));

    telescope_config.insert(CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH, QJsonValue(backslash_config));

    configuration->insert(CONFIG_KEY_TELESCOPE_SETTINGS, QJsonValue(telescope_config));

    emit configurationChanged();
}


void AutoGuidePanel::updateGuideSettingsArcsecPixel() {

    autoguideSettings.arcsecPerPixelRa = ui->le_arcsecPixelRa->text().toDouble();
    autoguideSettings.arcsecPerPixelDec = ui->le_arcsecPixelDec->text().toDouble();

    QJsonObject autoguide_config = configuration->value(CONFIG_KEY_AUTOGUIDE).toObject();

    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_ARCSEC_PER_PIXEL_RA, QJsonValue((double)autoguideSettings.arcsecPerPixelRa));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_ARCSEC_PER_PIXEL_DEC, QJsonValue((double)autoguideSettings.arcsecPerPixelDec));

    configuration->insert(CONFIG_KEY_AUTOGUIDE, QJsonValue(autoguide_config));

    emit configurationChanged();
}

void AutoGuidePanel::updateGuideSettingsMinimumInterval() {

    autoguideSettings.minimumSecondsIntervalRa = ui->le_minimumIntervalRa->text().toDouble();
    autoguideSettings.minimumSecondsIntervalDec = ui->le_minimumIntervalDec->text().toDouble();

    QJsonObject autoguide_config = configuration->value(CONFIG_KEY_AUTOGUIDE).toObject();

    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_MINIMUM_SECONDS_INTERVAL_RA, QJsonValue((double)autoguideSettings.minimumSecondsIntervalRa));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_MINIMUM_SECONDS_INTERVAL_DEC, QJsonValue((double)autoguideSettings.minimumSecondsIntervalDec));

    configuration->insert(CONFIG_KEY_AUTOGUIDE, QJsonValue(autoguide_config));

    emit configurationChanged();
}

void AutoGuidePanel::updateGuideSettingsMinimumShift() {

    autoguideSettings.minimumArcsecShiftRa  = ui->le_minimumArcsecShiftRa->text().toDouble();
    autoguideSettings.minimumArcsecShiftDec = ui->le_minimumArcsecShiftDec->text().toDouble();

    QJsonObject autoguide_config = configuration->value(CONFIG_KEY_AUTOGUIDE).toObject();

    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_MINIMUM_ARCSEC_SHIFT_RA, QJsonValue((double)autoguideSettings.minimumArcsecShiftRa));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_MINIMUM_ARCSEC_SHIFT_DEC, QJsonValue((double)autoguideSettings.minimumArcsecShiftDec));

    configuration->insert(CONFIG_KEY_AUTOGUIDE, QJsonValue(autoguide_config));

    emit configurationChanged();
}

void AutoGuidePanel::updateGuideSettingsAxes() {

    autoguideSettings.mirror_ra     = ui->cb_mirrorRA->isChecked();
    autoguideSettings.mirror_dec    = ui->cb_mirrorDec->isChecked();
    autoguideSettings.axes_rotation = ui->cb_axesRotation->isChecked();

    QJsonObject autoguide_config = configuration->value(CONFIG_KEY_AUTOGUIDE).toObject();

    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_MIRROR_RA, QJsonValue((bool)autoguideSettings.mirror_ra));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_MIRROR_DEC, QJsonValue((bool)autoguideSettings.mirror_dec));
    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_AXES_ROTATION, QJsonValue((bool)autoguideSettings.axes_rotation));

    configuration->insert(CONFIG_KEY_AUTOGUIDE, QJsonValue(autoguide_config));

    emit configurationChanged();
}

void AutoGuidePanel::updateGuideSettingsStarCogPower() {

    autoguideSettings.starGogPower = ui->sb_starCogPower->value();

    QJsonObject autoguide_config = configuration->value(CONFIG_KEY_AUTOGUIDE).toObject();

    autoguide_config.insert(CONFIG_KEY_AUTOGUIDE_STAR_COG_POWER, QJsonValue((qreal)autoguideSettings.starGogPower));

    configuration->insert(CONFIG_KEY_AUTOGUIDE, QJsonValue(autoguide_config));

    emit configurationChanged();
}

void AutoGuidePanel::updateBackgroundValuesTimeRange(int value) {
    backgroundValuesChartTimeRange = value;
}

void AutoGuidePanel::updateStarCenterPositionTimeRange(int value) {
    starCenterPositionChartTimeRange = value;
}

void AutoGuidePanel::connectTelescope() {

    logger->logDebug("Request connection");

    if (telescopeClient == NULL) {
        logger->logDebug("Init object");
        telescopeClient = new Telescope(telescopeParams.serverAddress, telescopeParams.serverPort, logger);
        connect(telescopeClient, &Telescope::telescopeConnected, this, &AutoGuidePanel::updateTelescopeConnectionStatus);
        telescopeClient->doConnect();

        connect(telescopeClient, &Telescope::positionAvailable, this, &AutoGuidePanel::updateTelescopePosition);

        ui->pb_startAutoguide->setEnabled(true);
        ui->pb_stopAutoguide->setEnabled(true);

    }
}

void AutoGuidePanel::disconnectTelescope() {

    ui->pb_startAutoguide->setDisabled(true);
    ui->pb_stopAutoguide->setDisabled(true);

    stopAutoguide();

    if (telescopeClient != NULL) {
        telescopeClient->doDisconnect();
        delete(telescopeClient);
        telescopeClient = NULL;
        ui->lb_telescopeConnectionStatus->setText("OFFLINE");
        ui->lb_telescopeConnectionStatus->setStyleSheet("QLabel { background-color : red;}");
    }
}

void AutoGuidePanel::updateTelescopeConnectionStatus() {

    ui->lb_telescopeConnectionStatus->setText("ONLINE");
    ui->lb_telescopeConnectionStatus->setStyleSheet("QLabel { background-color : green;}");
}

void AutoGuidePanel::getTelescopePosition() {

    if (telescopeClient != NULL) {
        telescopeClient->getCoords();
    }
}

void AutoGuidePanel::updateTelescopePosition(Telescope::RaDecCoords coords) {

    logger->logInfo("New position: " + QString::number(coords.ra) + " " + QString::number(coords.dec));

    telescopePosition = coords;

    ui->le_telescopePositionRA->setText(formatRa(coords.ra));
    ui->le_telescopePositionDEC->setText(formatDec(coords.dec));
}

void AutoGuidePanel::moveTelescope(double ra, double dec) {

    if (telescopeClient != NULL) {
        logger->logInfo("Telescope move: " + QString::number(ra) + " " + QString::number(dec));
        telescopeClient->deltaMove(ra, dec);
    }
}

void AutoGuidePanel::guideMoveTelescope(double raPixelShift, double decPixelShift) {

    if (!autoguideEnabled) {
        return ;
    }

    double tmp;
    double raArcsecShift = 0;
    double decArcsecShift = 0;
    double deltaDec;
    double deltaRa;

    // rotate axes
    if (ui->cb_axesRotation->isChecked()) {
        tmp = raPixelShift;
        raPixelShift = decPixelShift;
        decPixelShift = tmp;
    }

    // get current timestamp
    qint64 now = QDateTime::currentSecsSinceEpoch();

    // RA: convertion from pixel to arcsec and check last correction time
    if (now > (lastAutoguideCorrectionRa + autoguideSettings.minimumSecondsIntervalRa)) {
        raArcsecShift = raPixelShift * ui->le_arcsecPixelRa->text().toDouble();
        lastAutoguideCorrectionRa = now;
    }

    // RA: check if the shift is greater than the threshold
    if (qFabs(raArcsecShift) < autoguideSettings.minimumArcsecShiftRa) {
        raArcsecShift = 0;
    }

    // DEC: convertion from pixel to arcsec and check last correction time
    if (now > (lastAutoguideCorrectionDec + autoguideSettings.minimumSecondsIntervalDec)) {
        decArcsecShift = decPixelShift * ui->le_arcsecPixelDec->text().toDouble();
        lastAutoguideCorrectionDec = now;
    }

    // DEC: check if the shift is greater than the threshold
    if (qFabs(decArcsecShift) < autoguideSettings.minimumArcsecShiftDec) {
        decArcsecShift = 0;
    }

    // arcsec to radians convertion
    deltaRa  = raArcsecShift / 3600 / 180 * M_PI;
    deltaDec = decArcsecShift / 3600 / 180 * M_PI;

    deltaRa *= cos(deltaDec);

    if (deltaRa != 0 || deltaDec != 0) {

        if (ui->cb_mirrorRA) {
            deltaRa *= -1;
        }

        if (ui->cb_mirrorDec) {
            deltaDec *= -1;
        }

        moveTelescope(deltaRa, deltaDec);
    }
}

void AutoGuidePanel::moveTelescopeNorth() {

    double deltaDec = ui->le_northSteps->text().toDouble();
    deltaDec = deltaDec / 3600 / 180 * M_PI;

    if (lastTelescopeMoveDirection == TelescopeDirection::SOUTH) {
        double backslash = ui->le_northBackslash->text().toDouble();
        backslash = backslash / 3600 / 180 * M_PI;
        deltaDec += backslash;
    }
    lastTelescopeMoveDirection = TelescopeDirection::NORTH;

    logger->logDebug("Move NORTH");

    moveTelescope(0, deltaDec);
}

void AutoGuidePanel::moveTelescopeSouth() {

    double deltaDec = ui->le_southSteps->text().toDouble() * (-1);
    deltaDec = deltaDec / 3600 / 180 * M_PI;

    if (lastTelescopeMoveDirection == TelescopeDirection::NORTH) {
        double backslash = ui->le_southBackslash->text().toDouble();
        backslash = backslash / 3600 / 180 * M_PI;
        deltaDec += backslash;
    }
    lastTelescopeMoveDirection = TelescopeDirection::SOUTH;

    logger->logDebug("Move SOUTH");

    moveTelescope(0, deltaDec);
}

void AutoGuidePanel::moveTelescopeEast() {

    double deltaRa = ui->le_eastSteps->text().toDouble();
    deltaRa = deltaRa / 3600 / 180 * M_PI;

    if (lastTelescopeMoveDirection == TelescopeDirection::WEST) {
        double backslash = ui->le_eastBackslash->text().toDouble();
        backslash = backslash / 3600 / 180 * M_PI;
        deltaRa += backslash;
    }
    lastTelescopeMoveDirection = TelescopeDirection::EAST;

    logger->logDebug("Move EAST");

    moveTelescope(deltaRa, 0);
}

void AutoGuidePanel::moveTelescopeWest() {

    double deltaRa = ui->le_westSteps->text().toDouble() * (-1);
    deltaRa = deltaRa / 3600 / 180 * M_PI;

    if (lastTelescopeMoveDirection == TelescopeDirection::EAST) {
        double backslash = ui->le_westBackslash->text().toDouble();
        backslash = backslash / 3600 / 180 * M_PI;
        deltaRa += backslash;
    }
    lastTelescopeMoveDirection = TelescopeDirection::WEST;

    logger->logDebug("Move WEST");

    moveTelescope(deltaRa, 0);
}

quint32 AutoGuidePanel::getBoxXCenter() {
    return guideBox.xCenter;
}

quint32 AutoGuidePanel::getBoxYCenter() {
    return guideBox.yCenter;
}

quint32 AutoGuidePanel::getBoxInnerSize() {
    return guideBox.innerSize;
}

quint32 AutoGuidePanel::getBoxOuterSize() {
    return guideBox.outerSize;
}

void AutoGuidePanel::setSpectrographFlipHorizontal(int value) {
    spectrographFlipHorizontal = value;
}

void AutoGuidePanel::setSpectrographFlipVertical(int value) {
    spectrographFlipVertical = value;
}

void AutoGuidePanel::startAutoguide() {
    autoguideEnabled = true;
    ui->lb_autoguideStatus->setText("STARTED");
    ui->lb_autoguideStatus->setStyleSheet("QLabel { background-color : green;}");
}

void AutoGuidePanel::stopAutoguide() {
    autoguideEnabled = false;
    ui->lb_autoguideStatus->setText("STOPPED");
    ui->lb_autoguideStatus->setStyleSheet("QLabel { background-color : red;}");
}

QString AutoGuidePanel::formatRa(double ra) {

    int ra_h;
    int ra_m;
    int ra_s;

    ra_h = (int)(ra / M_PI * 12);
    ra_m = (int)(ra / M_PI * 12 * 60) % 60;
    ra_s = (int)(ra / M_PI * 12 * 3600) % 60;

    return QString("%1h %2m %3s").arg(ra_h).arg(ra_m).arg(ra_s);
}

QString AutoGuidePanel::formatDec(double dec) {

    int dec_d;
    int dec_m;
    int dec_s;

    dec_d = (int)(dec / M_PI * 180);
    dec_m = abs((int)(dec / M_PI * 180 * 60) % 60);
    dec_s = abs((int)(dec / M_PI * 180 * 3600) % 60);

    return QString("%1° %2' %3\"").arg(dec_d).arg(dec_m).arg(dec_s);
}

