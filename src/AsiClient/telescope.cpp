#include "telescope.h"

#include <string.h>

Telescope::Telescope(QString socket_address, quint16 socket_port, LogViewer *mainLogger, QObject *parent) : QObject(parent)
  , logger(mainLogger)
  , telescopeAddress(socket_address)
  , telescopePort(socket_port)
  , telescopeConnector(NULL)
{

}

void Telescope::doConnect() {

    telescopeConnector = new TelescopeConnector(telescopeAddress, telescopePort, logger);
    connect(telescopeConnector, &TelescopeConnector::telescopeConnected, this, &Telescope::telescopeConnectedState);
    connect(telescopeConnector, &TelescopeConnector::telescopeMessageAvailable, this, &Telescope::handleIncomingMessage);
    telescopeConnector->start();

}

void Telescope::doDisconnect() {

    if (telescopeConnector->isRunning()) {
        telescopeConnector->requestDisconnection();
        telescopeConnector->wait();
    }

}

void Telescope::sendCommand(TelescopeCommand command) {

    TelescopeNetMessage *message = new TelescopeNetMessage();

    message->setPsrc("client");
    message->setPdest("server");
    message->setPayload(command.par);
    message->setCommand(command.cmd);

    message->setCrc();

    telescopeConnector->newMessage(message);
}

void Telescope::connected() {

}

void Telescope::disconnected() {

}

void Telescope::getError(QAbstractSocket::SocketError socketError) {
    logger->logError("(Telescope) " + QString(socketError));
}

void Telescope::deltaMove(double ra_step, double dec_step) {

    TelescopeCommand command;
    command.cmd = TelescopeNetMessage::Instructions::AM_DELTA_MOVE;
    command.par = QString("%1 %2").arg(ra_step, 0, 'f', 8).arg(dec_step, 0, 'f', 8);
    sendCommand(command);

}

void Telescope::getStatus() {

    TelescopeCommand command;
    command.cmd = TelescopeNetMessage::Instructions::AM_GET_STATUS;

}

void Telescope::getCoords() {

    TelescopeCommand command;
    command.cmd = TelescopeNetMessage::Instructions::AM_GET_COORDS;
    sendCommand(command);
}

void Telescope::telescopeConnectedState() {
    emit telescopeConnected();
}

void Telescope::handleIncomingMessage(QByteArray msg) {

    TelescopeNetMessage::Message *message = new TelescopeNetMessage::Message();
    memcpy(message, msg.data(), sizeof(TelescopeNetMessage::Message));

    char *payload = new char(message->size);
    payload = msg.remove(0, 60).data();

    logger->logDebug("(Telescope) Message code: " + QString::number(message->code));

    switch (message->code) {
        case TelescopeNetMessage::Instructions::AM_GET_COORDS:

            logger->logDebug("(Telescope) Payload: " + QString(payload));

            QStringList data = QString(payload).split(" ");

            double ra, dec;
            if (data.count() == 2) {
                ra = data.at(0).toDouble();
                dec = data.at(1).toDouble();
            } else {
                ra = -9999;
                dec = -9999;
            }

            logger->logDebug("(Telescope) Received RA: " + QString::number(ra) + " DEC: " + QString::number(dec));

            RaDecCoords coords;
            coords.ra  = ra;
            coords.dec = dec;

            emit positionAvailable(coords);

            break;
    }
}
