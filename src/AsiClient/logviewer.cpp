#include "logviewer.h"
#include "ui_logviewer.h"

#include <QDateTime>
#include <QDebug>

LogViewer::LogViewer(QWidget *parent) : QWidget(parent)
    , ui(new Ui::LogViewer)
{
    ui->setupUi(this);

    qRegisterMetaType<QTextCursor>("QTextCursor");

    ui->le_logContainer->setFontFamily("Courier New");

    ui->cb_logLevels->addItem("TRACE");
    ui->cb_logLevels->addItem("DEBUG");
    ui->cb_logLevels->addItem("INFO");
    ui->cb_logLevels->addItem("WARNING");
    ui->cb_logLevels->addItem("ERROR");

    // set DEBUG as default log level
    ui->cb_logLevels->setCurrentIndex(1);
    defaultLogLevel = LogLevel::DEBUG;

    connect(ui->cb_logLevels, SIGNAL(currentIndexChanged(int)), this, SLOT(updateDefaultLogLevel(int)));
}

LogViewer::~LogViewer()
{
    delete ui;
}

void LogViewer::updateDefaultLogLevel(int index) {

    switch (index) {
        case 0:
            defaultLogLevel = LogLevel::TRACE;
            break;
        case 1:
            defaultLogLevel = LogLevel::DEBUG;
            break;
        case 2:
            defaultLogLevel = LogLevel::INFO;
            break;
        case 3:
            defaultLogLevel = LogLevel::WARNING;
            break;
        case 4:
            defaultLogLevel = LogLevel::ERROR;
            break;
    }
}

void LogViewer::setLogLevel(LogLevel level) {
    defaultLogLevel = level;
}

void LogViewer::logTrace(QString msg) {
    logMessage(LogLevel::TRACE, msg);
}

void LogViewer::logDebug(QString msg) {
    logMessage(LogLevel::DEBUG, msg);
}

void LogViewer::logInfo(QString msg) {
    logMessage(LogLevel::INFO, msg);
}

void LogViewer::logWarning(QString msg) {
    logMessage(LogLevel::WARNING, msg);
}

void LogViewer::logError(QString msg) {
    logMessage(LogLevel::ERROR, msg);
}

void LogViewer::logMessage(LogLevel level, QString msg) {

    if (level >= defaultLogLevel) {

        QString logString = QString("");

        QDateTime now = QDateTime::currentDateTime();

        logString.append(now.toString(Qt::ISODate));
        logString.append(" ");
        logString.append("[");

        switch (level) {
            case TRACE:
                logString.append("  TRACE  ");
                break;
            case INFO:
                logString.append("  INFO   ");
                break;
            case DEBUG:
                logString.append("  DEBUG  ");
                break;
            case WARNING:
                logString.append(" WARNING ");
                break;
            case ERROR:
                logString.append("  ERROR  ");
                break;
        }

        logString.append("]");
        logString.append(" ");
        logString.append(msg);

        ui->le_logContainer->append(logString);
        ui->le_logContainer->ensureCursorVisible();

    }
}
