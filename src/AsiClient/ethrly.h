#ifndef ETHRLY_H
#define ETHRLY_H

#include <QObject>
#include <QTcpSocket>

class EthRly : public QObject {

    Q_OBJECT

public:

    enum RelayStatus { unknown = 2, on = 1, off = 0 };

    struct BoardStatus {
            RelayStatus relay0;
            RelayStatus relay1;
            RelayStatus relay2;
            RelayStatus relay3;
            RelayStatus relay4;
            RelayStatus relay5;
            RelayStatus relay6;
            RelayStatus relay7;


            BoardStatus() {
                relay0 = RelayStatus::unknown;
                relay1 = RelayStatus::unknown;
                relay2 = RelayStatus::unknown;
                relay3 = RelayStatus::unknown;
                relay4 = RelayStatus::unknown;
                relay5 = RelayStatus::unknown;
                relay6 = RelayStatus::unknown;
                relay7 = RelayStatus::unknown;
            }

            friend bool operator!= (const BoardStatus& a, const BoardStatus& b) {

                return (a.relay0 != b.relay0 ||
                        a.relay1 != b.relay1 ||
                        a.relay2 != b.relay2 ||
                        a.relay3 != b.relay3 ||
                        a.relay4 != b.relay4 ||
                        a.relay5 != b.relay5 ||
                        a.relay6 != b.relay6 ||
                        a.relay7 != b.relay7);
            }
        };

    explicit EthRly(QString board_address, quint16 board_port, QObject *parent = nullptr);
    ~EthRly();

    void connect();
    void disconnect();

    void turnRelayOn(quint8 relay_num);
    void turnRelayOff(quint8 relay_num);
    BoardStatus getRelayStatus();

private:
    QString ipAddress;
    quint16 port;

    QTcpSocket *socket;

    QByteArray write(QByteArray command, bool expected_output);

    const int timeout = 2 * 1000;

    BoardStatus lastBoardStatus;

    bool endWithTimeout;

signals:
    void boardStatusChanged(EthRly::BoardStatus newStatus);

};

Q_DECLARE_METATYPE(EthRly::BoardStatus)

#endif // ETHRLY_H
