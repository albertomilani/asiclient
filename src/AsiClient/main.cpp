#include "asiclient.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<EthRly::BoardStatus>();
    AsiClient w;
    QObject::connect(&w, &AsiClient::exited, &a, &QApplication::quit);
    w.start();
    w.show();
    return a.exec();
}
