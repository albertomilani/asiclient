#ifndef TELESCOPE_MESSAGE_H
#define TELESCOPE_MESSAGE_H

#include <QObject>

class TelescopeNetMessage : public QObject {

    Q_OBJECT

public:

    TelescopeNetMessage();

    enum Instructions {
        AM_TEST = 0,
        AM_ACK = 1,
        AM_INIT_DEVICES = 2,
        AM_EXPOSE = 3,
        AM_SEND_IMAGE = 4,
        AM_GET_STATUS = 5,
        AM_SEND_STATUS = 6,
        AM_GOTO = 7,
        AM_MOVE = 8,
        AM_DELTA_MOVE = 9,
        AM_GOTO_OBJECT = 10,
        AM_SET_COOLER = 11,
        AM_WARMUP = 12,
        AM_FOCUS = 13,
        AM_POLAR = 14,
        AM_LAND = 15,
        AM_SHUTDOWN = 16,
        AM_SYNC = 17,
        AM_BINNING = 18,
        AM_STEPS_MOVE = 19,
        AM_DOME_COMMAND = 20,
        AM_TEL_PARK = 21,
        AM_FOCUS_GOTO = 22,
        AM_FOCUS_ZERO = 23,
        AM_GET_COORDS = 24,
        AM_NULL,
    };

#pragma pack(push, 1)

struct Message {
    quint32 stx;
    quint32 type;
    quint32 msrc;
    char psrc[12];
    quint32 mdest;
    char pdest[12];
    quint32 size;
    quint32 code;
    //void *chan;
    quint32 chan;
    quint32 crc;
    quint32 hcrc;
    // payload should be appended here, as last field
};

#pragma pack(pop)

    QByteArray getMessage();

    int getSize();

    void setStx(int stx);
    void setType(int type);
    void setMsrc(int msrc);
    void setPsrc(QString psrc);
    void setMdest(int mdest);
    void setPdest(QString pdest);

    void setCommand(Instructions command);
    void setPayload(QString payload);

    void setCrc();

    void setMessageFromBuffer(QByteArray *buffer);

private:

    Message *message = nullptr;
    QString messagePayload;

    int messageCrc(unsigned char *b, int n);

};


#endif // TELESCOPE_MESSAGE_H
