#ifndef CONFIG_KEYS_H
#define CONFIG_KEYS_H

#define CONFIG_KEY_CAMERAS "cameras"

#define CONFIG_KEY_CAMERA_SPECTROGRAPH "spectrograph"
#define CONFIG_KEY_CAMERA_SPECTROGRAPH_IP_ADDRESS "ip_address"
#define CONFIG_KEY_CAMERA_SPECTROGRAPH_PORT "port"
#define CONFIG_KEY_CAMERA_SPECTROGRAPH_ENABLED "enabled"

#define CONFIG_KEY_CAMERA_FIELD "field"
#define CONFIG_KEY_CAMERA_FIELD_IP_ADDRESS "ip_address"
#define CONFIG_KEY_CAMERA_FIELD_PORT "port"
#define CONFIG_KEY_CAMERA_FIELD_ENABLED "enabled"

#define CONFIG_KEY_CAMERA_DOME "dome"
#define CONFIG_KEY_CAMERA_DOME_IP_ADDRESS "ip_address"
#define CONFIG_KEY_CAMERA_DOME_PORT "port"
#define CONFIG_KEY_CAMERA_DOME_ENABLED "enabled"

#define CONFIG_KEY_CROSSHAIR1 "crosshair1"
#define CONFIG_KEY_CROSSHAIR2 "crosshair2"

#define CONFIG_KEY_CROSSHAIR_X "x"
#define CONFIG_KEY_CROSSHAIR_Y "y"
#define CONFIG_KEY_CROSSHAIR_ENABLED "enabled"
#define CONFIG_KEY_CROSSHAIR_HOLE_WIDTH "hole_width"

#define CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP "spectrograph_image_flip"
#define CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_HORIZONTAL "horizontal"
#define CONFIG_KEY_SPECTROGRAPH_IMAGE_FLIP_VERTICAL "vertical"

#define CONFIG_KEY_FIELD_IMAGE_FLIP "field_image_flip"
#define CONFIG_KEY_FIELD_IMAGE_FLIP_HORIZONTAL "horizontal"
#define CONFIG_KEY_FIELD_IMAGE_FLIP_VERTICAL "vertical"

#define CONFIG_KEY_FIELD_IMAGE_GRID "field_image_grid"
#define CONFIG_KEY_FIELD_IMAGE_GRID_X_CENTER "x_center"
#define CONFIG_KEY_FIELD_IMAGE_GRID_Y_CENTER "y_center"
#define CONFIG_KEY_FIELD_IMAGE_GRID_H_SPACING "h_spacing"
#define CONFIG_KEY_FIELD_IMAGE_GRID_V_SPACING "v_spacing"
#define CONFIG_KEY_FIELD_IMAGE_GRID_ENABLED "enabled"

#define CONFIG_KEY_RELAY_BOARD "relay_board"
#define CONFIG_KEY_RELAY_BOARD_IP_ADDRESS "ip_address"
#define CONFIG_KEY_RELAY_BOARD_PORT "port"

#define CONFIG_KEY_AUTOGUIDE "autoguide"
#define CONFIG_KEY_AUTOGUIDE_X_CENTER "x_center"
#define CONFIG_KEY_AUTOGUIDE_Y_CENTER "y_center"
#define CONFIG_KEY_AUTOGUIDE_INNER_BOX_CENTER "inner_box_size"
#define CONFIG_KEY_AUTOGUIDE_OUTER_BOX_CENTER "outer_box_size"
#define CONFIG_KEY_AUTOGUIDE_ARCSEC_PER_PIXEL_DEC "arcsec_per_pixel_dec"
#define CONFIG_KEY_AUTOGUIDE_ARCSEC_PER_PIXEL_RA "arcsec_per_pixel_ra"
#define CONFIG_KEY_AUTOGUIDE_MINIMUM_SECONDS_INTERVAL_DEC "minimum_seconds_interval_dec"
#define CONFIG_KEY_AUTOGUIDE_MINIMUM_SECONDS_INTERVAL_RA "minimum_seconds_interval_ra"
#define CONFIG_KEY_AUTOGUIDE_MINIMUM_ARCSEC_SHIFT_DEC "minimum_arcsec_shift_dec"
#define CONFIG_KEY_AUTOGUIDE_MINIMUM_ARCSEC_SHIFT_RA "minimum_arcsec_shift_ra"
#define CONFIG_KEY_AUTOGUIDE_MIRROR_RA "mirror_ra"
#define CONFIG_KEY_AUTOGUIDE_MIRROR_DEC "mirror_dec"
#define CONFIG_KEY_AUTOGUIDE_AXES_ROTATION "axes_rotation"
#define CONFIG_KEY_AUTOGUIDE_STAR_COG_POWER "star_cog_power"

#define CONFIG_KEY_TELESCOPE_SETTINGS "telescope_settings"

#define CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH "backslash"
#define CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_NORTH "north"
#define CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_SOUTH "south"
#define CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_EAST "east"
#define CONFIG_KEY_TELESCOPE_SETTINGS_BACKSLASH_WEST "west"

#define CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP "move_step"
#define CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_NORTH "north"
#define CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_SOUTH "south"
#define CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_EAST "east"
#define CONFIG_KEY_TELESCOPE_SETTINGS_MOVE_STEP_WEST "west"

#define CONFIG_KEY_TELESCOPE_SETTINGS_SERVER "server"
#define CONFIG_KEY_TELESCOPE_SETTINGS_SERVER_ADDRESS "address"
#define CONFIG_KEY_TELESCOPE_SETTINGS_SERVER_PORT "port"

#endif // CONFIG_KEYS_H
