#ifndef TELESCOPE_H
#define TELESCOPE_H

#include <cstddef>
#include <QObject>
#include <QAbstractSocket>
#include <QTcpSocket>

#include "telescope_message.h"
#include "telescope_connector.h"
#include "logviewer.h"

class Telescope : public QObject
{
    Q_OBJECT

public:
    explicit Telescope(QString socket_address, quint16 socket_port, LogViewer *mainLogger, QObject* parent = nullptr);

    void doConnect();
    void doDisconnect();

    void deltaMove(double ra_steps, double dec_steps);
    void getStatus();
    void getCoords();

    struct RaDecCoords {
        double ra;
        double dec;

        RaDecCoords() {
            ra  = 0.0;
            dec = 0.0;
        }

    } telescopePosition;

private:

    LogViewer *logger;

    QString telescopeAddress;
    quint16 telescopePort;

    struct TelescopeCommand {
        TelescopeNetMessage::Instructions cmd;
        QString par;
    };

    TelescopeConnector *telescopeConnector;

    void sendCommand(TelescopeCommand command);

public slots:
    void connected();
    void disconnected();
    void getError(QAbstractSocket::SocketError socketError);

private slots:
    void telescopeConnectedState();
    void handleIncomingMessage(QByteArray msg);

signals:
    void telescopeConnected();
    void positionAvailable(RaDecCoords coords);
};

#endif // TELESCOPE_H
